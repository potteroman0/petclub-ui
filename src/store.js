import { applyMiddleware, createStore } from "redux";
import { promiseMiddleware, localStorageMiddleware } from "./middleware";
import { composeWithDevTools } from "redux-devtools-extension";
import createReducers from "./reducer";

import { routerMiddleware } from "connected-react-router";
import { createBrowserHistory } from "history";

export const history = createBrowserHistory();
const myRouterMiddleware = routerMiddleware(history);
const middleware = applyMiddleware(
  myRouterMiddleware,
  promiseMiddleware,
  localStorageMiddleware
);

export const store = createStore(
  createReducers(history),
  composeWithDevTools(middleware)
);
