import React from "react";

import agent from "../../agent";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import {
  CHANGE_PROFILE_TAB,
  LOAD_RESOURCES,
  PROFILE_PAGE_LOADED,
  PROFILE_PAGE_UNLOADED,
} from "../../constants/actionTypes";
import profile from "../../reducers/user/profile";

import ArticleList from "../Articles/ArticleList";
import QuestionList from "../Questions/QuestionList";
import UserActions from "./UserActions";

const mapStateToProps = (state) => ({
  ...state.profile,
  currentUser: state.common.currentUser,
  user: state.profile.user,
  size: state.common.size,
  page: state.common.page,
});

const mapDispatchToProps = (dispatch) => ({
  onLoad: (payload) => dispatch({ type: PROFILE_PAGE_LOADED, payload }),
  onUnload: () => dispatch({ type: PROFILE_PAGE_UNLOADED }),
  onLoadResources: (payload) => dispatch({ type: LOAD_RESOURCES, payload }),
  onTabClick: (tab) => dispatch({ type: CHANGE_PROFILE_TAB, tab }),
});

const ContentList = (props) => {
  if (props.tab === "articles") {
    return (
      <ArticleList
        pager={agent.Articles.allPaginable}
        size={props.size}
        currentPage={props.currentPage}
        articles={props.articles}
        articlesCount={props.articlesCount}
        loading={false}
        currentUser={props.user}
        tab={"all"}
        section={"articles"}
      />
    );
  } else if (props.tab === "questions") {
    return (
      <QuestionList
        user={props.user}
        tab={"all"}
        size={props.size}
        pager={props.pager}
        questions={props.questions}
        loading={props.loading}
        questionsCount={props.questionsCount}
        currentPage={props.currentPage}
        section={"questions"}
      />
    );
  } else if (props.tab === "orders") {
    return null;
  }
  return null;
};

const AllArticlesTab = (props) => {
  const clickHandler = (ev) => {
    ev.preventDefault();
    props.onTabClick("articles");
  };
  return (
    <li className="nav-item">
      <a
        href=""
        className={props.tab === "articles" ? "nav-link active" : "nav-link"}
        onClick={clickHandler}
      >
        Articles
      </a>
    </li>
  );
};

const AllQuestionsTab = (props) => {
  const clickHandler = (ev) => {
    ev.preventDefault();
    props.onTabClick("questions");
  };
  return (
    <li className="nav-item">
      <a
        href=""
        className={props.tab === "questions" ? "nav-link active" : "nav-link"}
        onClick={clickHandler}
      >
        Questions
      </a>
    </li>
  );
};

class Profile extends React.Component {
  constructor() {
    super();
    this.state = {
      loadRes: false,
    };
  }

  componentWillMount() {
    this.props.onLoad(
      agent.Auth.getByUsername(this.props.match.params.username)
    );
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  loadResources(id, size, page) {
    if (!this.state.loadRes) {
      this.setState({ loadRes: true });
      this.props.onLoadResources(
        Promise.all([
          agent.Articles.getByUserId(id, size, page),
          agent.Questions.getByUserId(id, size, page),
          agent.Orders.getByUserId(id, size, page),
        ])
      );
    }
  }

  render() {
    const user = this.props.user;
    if (!user) {
      return null;
    }
    this.loadResources(user.id, this.props.size, this.props.page);
    return (
      <div>
        <div className="profile-page">
          <div className="profile-header">
            <div className="profile-details">
              <img
                src={user.imgUrl}
                className="profile-pic"
                alt={user.username}
              />
              <h4 className="heading">{user.username}</h4>
              <div className="user-action">
                <UserActions
                  canModify={
                    this.props.currentUser === null
                      ? false
                      : this.props.currentUser?.role === "ADMIN"
                  }
                />
              </div>
              <div class="stats">
                <div class="col-4">
                  <h4>
                    {this.props.articles ? this.props.articles.length : 0}
                  </h4>
                  <p>Artilces</p>
                </div>
                <div class="col-4">
                  <h4>
                    {this.props.questions ? this.props.questions.length : 0}
                  </h4>
                  <p>Questions</p>
                </div>
                <div class="col-4">
                  <h4>{this.props.orders ? this.props.orders.lenght : 0}</h4>
                  <p>Orders</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-9">
          <div className="feed-toggle">
            <ul className="nav nav-pills outline-active">
              <li className="Article_filter">
                <AllArticlesTab
                  tab={this.props.tab}
                  size={this.props.size}
                  page={this.props.page}
                  onTabClick={this.props.onTabClick}
                />

                <AllQuestionsTab
                  tab={this.props.tab}
                  size={this.props.size}
                  page={this.props.page}
                  onTabClick={this.props.onTabClick}
                />
              </li>
            </ul>
          </div>
          <ContentList
            tab={this.props.tab}
            user={this.props.user}
            size={this.props.size}
            currentPage={this.props.page}
            articles={this.props.articles}
            questions={this.props.questions}
            acticlesCount={5}
            questionsCount={5}
          />
        </div>
      </div>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Profile);
