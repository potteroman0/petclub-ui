import React from "react";

import ListErrors from "../ListErrors";

import { Link } from "react-router";
import agent from "../../agent";
import { connect } from "react-redux";
import SettingsForm from "./SettingsForm";

import {
  DELETE_USER_ACCOUNT,
  SETTINGS_PAGE_UNLOADED,
  LOGOUT,
} from "../../constants/actionTypes";

const mapStateToProps = (state) => ({
  ...state.settings,
  currentUser: state.common.currentUser,
});

const mapDispatchToProps = (dispatch) => ({
  onClickLogout: () => dispatch({ type: LOGOUT }),
  onClickDelete: (payload) => dispatch({ type: DELETE_USER_ACCOUNT, payload }),
  onUnload: () => dispatch({ type: SETTINGS_PAGE_UNLOADED }),
});

class Settings extends React.Component {
  deleteAccount = () => {
    this.props.onClickDelete(agent.Users.delete(this.props.currentUser.id));
  };
  render() {
    return (
      <div className="settings-page">
        <div className="container page">
          <div className="row">
            <h1 className="text-xs-center">Your Settings</h1>

            <ListErrors errors={this.props.errors}></ListErrors>

            <SettingsForm />

            <div className="acc-control-buttons">
              <button
                className="btn-secondary btn-outline-danger"
                onClick={this.props.onClickLogout}
              >
                Or click here to logout.
              </button>

              <button className=" btn-outline-del" onClick={this.deleteAccount}>
                Click here to delete account
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Settings);
