import React from "react";

import { Link } from "react-router-dom";
import agent from "../../agent";
import { connect } from "react-redux";

import { DELETE_ARTICLE } from "../../constants/actionTypes";

const mapDispatchToProps = (dispatch) => ({});

const UserActions = (props) => {
  if (props.canModify) {
    return (
      <span>
        <button className="btn-secondary btn-del">
          <i className="ion-trash-a"></i> Make moderator
        </button>
        <button className="btn-secondary btn-del">
          <i className="ion-trash-a"></i> Block
        </button>
      </span>
    );
  }

  return <span></span>;
};

export default connect(() => ({}), mapDispatchToProps)(UserActions);
