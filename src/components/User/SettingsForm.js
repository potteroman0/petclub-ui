import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDownload } from "@fortawesome/free-solid-svg-icons";
import {
  LOGOUT,
  SETTINGS_SAVED,
  UPDATE_FIELD_SETTINGS,
  SETTINGS_PAGE_UNLOADED,
  USER_IMAGE_FOLDER,
  SETTINGS_PAGE_LOADED,
  USER_IMAGE_LOAD,
} from "../../constants/actionTypes";

import agent from "../../agent";
import { connect } from "react-redux";

const mapStateToProps = (state) => ({
  ...state.settings,
  currentUser: state.common.currentUser,
});

const mapDispatchToProps = (dispatch) => ({
  onLoad: (payload) => dispatch({ type: SETTINGS_PAGE_LOADED, payload }),
  onUnload: () => dispatch({ type: SETTINGS_PAGE_UNLOADED }),
  onClickLogout: () => dispatch({ type: LOGOUT }),
  onSubmitForm: (user) =>
    dispatch({
      type: SETTINGS_SAVED,
      payload: agent.Users.update(
        user.id,
        user.username,
        user.password,
        user.email,
        user.phone,
        user.imgUrl
      ),
    }),
  onLoadImage: (payload) => dispatch({ type: USER_IMAGE_LOAD, payload }),
  onUpdateField: (key, value) =>
    dispatch({ type: UPDATE_FIELD_SETTINGS, key, value }),
});

class SettingsForm extends React.Component {
  constructor() {
    super();

    const updateFieldEvent = (field) => (ev) => {
      if (field === "imgUrl") {
        this.props.onLoadImage(
          agent.Photos.upload(USER_IMAGE_FOLDER, ev.target.files[0])
        );
      } else {
        this.props.onUpdateField(field, ev.target.value);
      }
    };

    this.changeImage = updateFieldEvent("imgUrl");
    this.changeUsername = updateFieldEvent("username");
    this.changeEmail = updateFieldEvent("email");
    this.changePassword = updateFieldEvent("password");
    this.changePhone = updateFieldEvent("phone");

    this.submitForm = (ev) => {
      ev.preventDefault();

      const user = {
        id: this.props.currentUser.id,
        username: this.props.username,
        phone: this.props.phone,
        email: this.props.email,
        password: this.props.password,
        imgUrl: this.props.imgUrl,
      };

      this.props.onSubmitForm(user);
    };
  }

  componentDidMount() {
    if (this.props.currentUser) {
      this.props.onLoad(this.props.currentUser);
    }
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    return (
      <form onSubmit={this.submitForm}>
        <fieldset>
          <fieldset>
            <label for="image" className="avatar-change">
              <FontAwesomeIcon icon={faDownload} />
              &nbsp;Change avatar
            </label>{" "}
            <input
              type="file"
              id="image"
              className="avatar-change-input"
              accept="image/png, image/jpeg"
              onChange={this.changeImage}
            />
          </fieldset>

          <fieldset className="form-group">
            <input
              className="form-control form-control-lg"
              type="text"
              placeholder="Username"
              value={this.props.username}
              onChange={this.changeUsername}
            />
          </fieldset>

          <fieldset className="form-group">
            <input
              className="form-control form-control-lg"
              type="text"
              placeholder="Phone"
              value={this.props.phone}
              onChange={this.changePhone}
            ></input>
          </fieldset>

          <fieldset className="form-group">
            <input
              className="form-control form-control-lg"
              type="email"
              placeholder="Email"
              value={this.props.email}
              onChange={this.changeEmail}
            />
          </fieldset>

          <fieldset className="form-group">
            <input
              className="form-control form-control-lg"
              type="password"
              placeholder="New Password"
              value={this.props.password}
              onChange={this.changePassword}
            />
          </fieldset>

          <button
            className="btn btn-lg btn-primary pull-xs-right"
            type="submit"
            disabled={this.props.inProgress}
          >
            Update Settings
          </button>
        </fieldset>
      </form>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsForm);
