import React from "react";

import agent from "../../agent";

const AddHashtag = (props) => {
  const [tag, setTag] = React.useState("input");
  if (props.user) {
    if (props.user.role === "ADMIN") {
      const watchForEnter = (ev) => {
        if (ev.keyCode === 13) {
          ev.preventDefault();
          props.onCreate(agent.Tags.create(tag));
        }
      };

      const changeTagInput = (ev) => {
        ev.preventDefault();
        setTag(ev.target.value);
      };

    return (
       
        <div className='addtag'>
      <input
      
        
        type="text"
        placeholder="Write a hashtag"
        
        onChange={changeTagInput}
        onKeyUp={watchForEnter}
      /></div>
        
      
    );
  }
  } return null;
};

const RemoveTag = (props) => {
  if (props.user) {
    if (props.user.role === "ADMIN") {
      const handleRemoveClick = (ev) => {
        ev.preventDefault();
        props.onDelete(props.tag.name, agent.Tags.delete(props.tag.id));
      };

      return (
        <div>
          <i className="ion-close-round" onClick={handleRemoveClick}></i>
        </div>
      );
    }
  }
  return null;
};

const Tags = (props) => {
  const tags = props.hashtags;
  if (tags) {
    return (
      <div className="tag-list">
        {tags.map((tag) => {
          const handleClick = (ev) => {
            ev.preventDefault();
            props.onClickTag(
              tag.name,
              agent.Articles.getByHashtag,
              agent.Articles.getByHashtag(tag.name, props.size, props.page)
            );
          };

          return (
            <a
              href=""
              className="tag-default tag-pill"
              key={tag.id}
              onClick={handleClick}
            >
              
              {tag.name+" "} 
              <RemoveTag
                user={props.user}
                tag={tag}
                onDelete={props.onDeleteTag}
              />
            </a>
          );
        })}
        <AddHashtag user={props.user} onCreate={props.onCreateTag} />
      </div>
    );
  } else {
    return <div>Loading Tags...</div>;
  }
};

export default Tags;
