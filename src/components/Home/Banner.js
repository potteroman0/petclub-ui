import React from "react";

const Banner = ({ appName, token }) => {
  if (token) {
    return null;
  }
  return (
    <div className="banner-green">
      <div className="container">
        <h1 className="logo-font">{appName.toLowerCase()}</h1>
        <p>All for your pet</p>
      </div>
    </div>
  );
};

export default Banner;
