import React from "react";
import "../style.scss";
import Login from "./Auth/Login";
import Register from "./Auth/Register";
import Header from "./Header";
import Home from "./Home";
import Article from "./Articles/Article";
import Articles from "./Articles";
import Settings from "./User/Settings";
import ArticleEditor from "./Articles/ArticleEditor";
import QuestionEditor from "../components/Questions/QuestionEditor";
import Cart from "../components/Cart";

import { store } from "../store";
import { push } from "react-router-redux";
import agent from "../agent";

import { Route, Switch } from "react-router-dom";
import { connect } from "react-redux";

import { APP_LOAD, REDIRECT } from "../constants/actionTypes";
import Questions from "./Questions";
import Question from "./Questions/Question";
import Profile from "./User/Profile";
import Advertisements from "./Advertisements";
import Items from "./Items";
import ItemForm from "./Items/ItemForm";
import Item from "./Items/Item";

const mapStateToProps = (state) => ({
  appName: state.common.appName,
  currentUser: state.common.currentUser,
  redirectTo: state.common.redirectTo,
});

const mapDispatchToProps = (dispatch) => ({
  onLoad: (payload, token) => dispatch({ type: APP_LOAD, payload, token }),
  onRedirect: () => dispatch({ type: REDIRECT }),
});

class App extends React.Component {
  componentDidMount() {
    const token = window.localStorage.getItem("jwt");
    if (token) {
      console.log("token", token);
      agent.setToken(token);
    }
    this.props.onLoad(token ? agent.Auth.getCurrent() : null, token);
  }

  static getDerivedStateFromProps(nextProps) {
    if (nextProps.redirectTo) {
      store.dispatch(push(nextProps.redirectTo));
      return { needRedirectTo: nextProps.redirectTo };
    } else return null;
  }

  componentDidUpdate() {
    if (this.props.redirectTo) {
      this.props.onRedirect();
    }
  }

  render() {
    return (
      <div>
        <Header
          currentUser={this.props.currentUser}
          appName={this.props.appName}
        />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/questions" component={Questions} />
          <Route path="/question-editor/:id" component={QuestionEditor} />
          <Route path="/question-editor" component={QuestionEditor} />
          <Route path="/questions/:id" component={Question} />

          <Route path="/articles" component={Articles} />
          <Route path="/article/:id" component={Article} />
          <Route path="/editor/:id" component={ArticleEditor} />
          <Route path="/editor" component={ArticleEditor} />

          <Route path="/advertisements" component={Advertisements} />
          <Route path="/shop" component={Items} />
          <Route path="/item-editor/:id" component={ItemForm} />
          <Route path="/item-editor" component={ItemForm} />
          <Route path="/item/:id" component={Item} />
          <Route path="/cart" component={Cart} />

          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/settings" component={Settings} />
          <Route path="/:username" component={Profile} />
        </Switch>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
