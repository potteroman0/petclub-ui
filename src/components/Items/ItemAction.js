import React from "react";

import { Link } from "react-router-dom";
import agent from "../../agent";
import { connect } from "react-redux";
import { DELETE_ITEM } from "../../constants/actionTypes";

const mapDispatchToProps = (dispatch) => ({
  onClickDelete: (payload) => dispatch({ type: DELETE_ITEM, payload }),
});

const ItemAction = (props) => {
  const item = props.item;
  const del = () => {
    props.onClickDelete(agent.Items.delete(item.id));
  };
  if (props.canModify) {
    return (
      <span>
        <Link to={`/item-editor/${item.id}`} className="btn-secondary btn-edit">
          <i className="ion-edit"></i> Edit Item
        </Link>

        <button className="btn-secondary btn-del" onClick={del}>
          <i className="ion-trash-a"></i> Delete Item
        </button>
      </span>
    );
  }

  return <span></span>;
};

export default connect(() => ({}), mapDispatchToProps)(ItemAction);
