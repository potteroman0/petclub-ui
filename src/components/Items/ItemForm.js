import React from "react";

import {
  IMAGE_LOAD,
  UPDATE_FIELD_ITEM,
  ITEM_SAVED,
  ITEM_EDITOR_PAGE_LOADED,
  ITEM_EDITOR_PAGE_UNLOADED,
  ITEMS_IMAGE_FOLDER,
  DEFAULT_ITEM_ICON,
} from "../../constants/actionTypes";
import { faDownload } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import agent from "../../agent";
import { connect } from "react-redux";

const mapStateToProps = (state) => ({
  ...state.itemEditor,
  currentUser: state.common.currentUser,
});

const mapDispatchToProps = (dispatch) => ({
  onLoad: (payload) => dispatch({ type: ITEM_EDITOR_PAGE_LOADED, payload }),
  onUnload: () => dispatch({ type: ITEM_EDITOR_PAGE_UNLOADED }),

  onSubmitForm: (payload) => dispatch({ type: ITEM_SAVED, payload }),
  onLoadImage: (payload) => dispatch({ type: IMAGE_LOAD, payload }),
  onUpdateField: (key, value) =>
    dispatch({ type: UPDATE_FIELD_ITEM, key, value }),
});

class ItemForm extends React.Component {
  constructor() {
    super();

    const updateFieldEvent = (field) => (ev) => {
      if (field === "imgUrl") {
        this.props.onLoadImage(
          agent.Photos.upload(ITEMS_IMAGE_FOLDER, ev.target.files[0])
        );
      } else {
        this.props.onUpdateField(field, ev.target.value);
      }
    };

    this.changeTitle = updateFieldEvent("title");
    this.changeDescription = updateFieldEvent("description");
    this.changeAmount = updateFieldEvent("amount");
    this.changePrice = updateFieldEvent("price");
    this.changeCategory = updateFieldEvent("category");
    this.changeType = updateFieldEvent("petType");
    this.changeImage = updateFieldEvent("imgUrl");

    this.submitForm = (ev) => {
      ev.preventDefault();

      const item = {
        title: this.props.title,
        description: this.props.description,
        amount: this.props.amount,
        price: this.props.price,
        type: this.props.category.toUpperCase(),
        petType: this.props.petType.toUpperCase(),
        imgUrl:
          this.props.imgUrl === "" ? DEFAULT_ITEM_ICON : this.props.imgUrl,
      };

      const promise = this.props.itemId
        ? agent.Items.update(
            this.props.itemId,
            item.title,
            item.description,
            item.amount,
            item.price,
            item.imgUrl,
            item.type,
            item.petType
          )
        : agent.Items.create(
            item.title,
            item.description,
            item.amount,
            item.price,
            item.imgUrl,
            item.type,
            item.petType
          );

      this.props.onSubmitForm(promise);
    };
  }

  componentDidMount() {
    if (this.props.match.params.id) {
      this.props.onLoad(
        Promise.all([
          agent.Items.getById(this.props.match.params.id),
          agent.Items.getItemCategories(),
          agent.Items.getItemPetTypes(),
        ])
      );
    }
    this.props.onLoad(
      Promise.all([
        null,
        agent.Items.getItemCategories(),
        agent.Items.getItemPetTypes(),
      ])
    );
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    console.log(this.props.categories);
    console.log(this.props.types);
    return (
      <div className="editor-page">
        <div className="container page">
          <form onSubmit={this.submitForm}>
            <fieldset>
              <div className="row">
                <fieldset>
                  <label for="image" className="avatar-change">
                    <FontAwesomeIcon icon={faDownload} />
                    &nbsp;Insert image
                  </label>
                  <input
                    type="file"
                    id="image"
                    className="avatar-change-input"
                    accept="image/png, image/jpeg"
                    onChange={this.changeImage}
                  />
                </fieldset>

                <fieldset className="form-group">
                  <input
                    className="form-control-title"
                    type="text"
                    placeholder="Title"
                    value={this.props.title}
                    onChange={this.changeTitle}
                  />
                </fieldset>
              </div>

              <fieldset className="form-group">
                <textarea
                  className="form-control"
                  type="text"
                  placeholder="Description"
                  value={this.props.description}
                  onChange={this.changeDescription}
                ></textarea>
              </fieldset>

              <div className="row">
                <fieldset className="form-group">
                  <input
                    className="form-control"
                    type="number"
                    placeholder="Amount"
                    value={this.props.amount}
                    onChange={this.changeAmount}
                  />
                </fieldset>
              </div>

              <fieldset className="form-group">
                <input
                  className="form-control"
                  type="number"
                  placeholder="Price"
                  value={this.props.price}
                  onChange={this.changePrice}
                />
              </fieldset>

              <div className="row">
                <fieldset className="category-list">
                  <label for="categories">Choose a item category</label>

                  <select
                    name="category"
                    id="category"
                    onChange={this.changeCategory}
                  >
                    <option>----</option>
                    {this.props.categories.map((category) => (
                      <option
                        selected={
                          category.toUpperCase() === this.props.category
                        }
                        value={category}
                      >
                        {category}
                      </option>
                    ))}
                  </select>
                </fieldset>
              </div>

              <div className="row">
                <fieldset className="category-list">
                  <label for="types">Choose a pet type</label>

                  <select
                    name="petType"
                    id="petType"
                    onChange={this.changeType}
                  >
                    <option>----</option>
                    {this.props.types.map((type) => (
                      <option
                        selected={type.toUpperCase() === this.props.petType}
                        value={type}
                      >
                        {type}
                      </option>
                    ))}
                  </select>
                </fieldset>
              </div>

              <button
                className="btn-move btn-publish"
                type="submit"
                disabled={this.props.inProgress}
              >
                Add Item
              </button>
            </fieldset>
          </form>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemForm);
