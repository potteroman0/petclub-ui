import React from "react";
import agent from "../../agent";

const ItemPetsType = (props) => {
  const pets = props.types;
  if (pets) {
    return (
      <div className="type-list">
        {pets.map((pet) => {
          const handleClick = (ev) => {
            ev.preventDefault();
            // props.onClickCategory(
            //   category,
            //   agent.Questions.getByCategory,
            //   agent.Questions.getByCategory(category, props.size, props.page)
            // );
          };

          return (
            <a href="" className="tag-default tag-pill" onClick={handleClick}>
              {pet}
            </a>
          );
        })}
      </div>
    );
  } else {
    return <div className="loading">Loading Categories...</div>;
  }
};

export default ItemPetsType;
