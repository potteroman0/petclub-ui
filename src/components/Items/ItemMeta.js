import React from "react";
import agent from "../../agent";

import ItemAction from "./ItemAction";

const ItemMeta = (props) => {
  const item = props.item;
  return (
    <div className="article-meta">
      {/* <div className="info">
        <img src={item.imgUrl} />
      </div> */}
      <div className="user-info">
        <ItemAction canModify={props.canModify} item={item} />
      </div>
    </div>
  );
};

export default ItemMeta;
