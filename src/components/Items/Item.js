import React from "react";

import agent from "../../agent";
import { connect } from "react-redux";

import ItemMeta from "./ItemMeta";
import {
  ITEM_PAGE_LOADED,
  ITEM_PAGE_UNLOADED,
  ADD_ITEM_TO_CART,
} from "../../constants/actionTypes";

const mapStateToProps = (state) => ({
  ...state.item,
  currentUser: state.common.currentUser,
});

const mapDispatchToProps = (dispatch) => ({
  onLoad: (payload) => dispatch({ type: ITEM_PAGE_LOADED, payload }),
  onUnload: () => dispatch({ type: ITEM_PAGE_UNLOADED }),
  onAddToCart: (payload) => dispatch({ type: ADD_ITEM_TO_CART, payload }),
});

class Item extends React.Component {
  componentDidMount() {
    this.props.onLoad(agent.Items.getById(this.props.match.params.id));
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    if (!this.props.item) {
      return null;
    }
    const item = this.props.item;
    const canModify =
      this.props.currentUser && this.props.currentUser.role === "ADMIN";
    const addToCart = (ev) => {
      this.props.onAddToCart(item);
    };
    return (
      <div className="one-item-page">
        <div className="banner">
          <div className="container">
            {/* <h1 className="article_title title-article-page">
              {this.props.item.title}
            </h1> */}
            <ItemMeta item={this.props.item} canModify={canModify} />
          </div>
        </div>

        <div className="item-container">
          <div class="left-column">
            <img src={item.imgUrl} class="active" />
          </div>

          <div className="right-column">
            {/* <h1>{item.title}</h1> */}
            <div className="product-description">
              {/* <span>{item.title}</span> */}
              <h1>{item.title}</h1>
              <p>{item.description}</p>
            </div>
            <div className="product-price">
              <span>{item.price + "$"}</span>
              <button className="cart-btn" onClick={addToCart}>
                Add to cart
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Item);
