import React from "react";

import agent from "../../agent";
import { connect } from "react-redux";

import Banner from "../Home/Banner";
import ItemsView from "./ItemsView";

import {
  ITEMS_PAGE_LOADED,
  ITEMS_PAGE_UNLOADED,
  APPLY_ITEM_CATEGORY_FILTER,
} from "../../constants/actionTypes";
import ItemCategories from "./ItemCategories";
import ItemPetsType from "./ItemPetsType";
import ItemFilterPanel from "./ItemFilterPanel";

const Promise = global.Promise;

const mapStateToProps = (state) => ({
  appName: state.common.appName,
  size: state.itemsList.size,
  page: state.common.page,
  tab: state.common.tab,
  categories: state.itemsList.categories,
  types: state.itemsList.types,
});

const mapDispatchToProps = (dispatch) => ({
  onLoad: (tab, pager, payload) =>
    dispatch({ type: ITEMS_PAGE_LOADED, tab, pager, payload }),
  onUnload: () => dispatch({ type: ITEMS_PAGE_UNLOADED }),
  onClickCategory: (category, pager, payload) =>
    dispatch({ type: APPLY_ITEM_CATEGORY_FILTER, category, pager, payload }),
});

class Items extends React.Component {
  componentDidMount() {
    const itemPromise = agent.Items.getAllItems;
    this.props.onLoad(
      this.props.tab,
      itemPromise,
      Promise.all([
        itemPromise(this.props.size, this.props.page),
        agent.Items.getItemCategories(),
        agent.Items.getItemPetTypes(),
      ])
    );
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    return (
      <div className="home-page">
        <Banner appName={this.props.appName} />

        <div className="container page">
          <div className="article_mainpage">
            <div className="ArticlesView">
              <ItemsView />
            </div>

            <div className="sidebar">
              {/* <div className="regular">
                <div className="tagsbar">
                  <p>Pet Types</p>
                  <ItemPetsType types={this.props.types} />
                </div>
              </div> */}
              <div className="regular">
                <div className="filter_panel">
                  <ItemFilterPanel />
                </div>
                <div className="tagsbar">
                  <p>Categories</p>
                  <ItemCategories
                    categories={this.props.categories}
                    onClickCategory={this.props.onClickCategory}
                    size={this.props.size}
                    page={this.props.page}
                  />
                </div>
                <br />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Items);
