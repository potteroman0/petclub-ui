import React from "react";
import ItemPreview from "./ItemPreview";
import ListPagination from "../ListPagination";

const ItemList = (props) => {
  if (!props.items) {
    return <div className="article-preview">Loading...</div>;
  }

  if (props.items.length === 0) {
    return <div className="article-preview">No items are here... yet.</div>;
  }

  const itemFromCart = props.cartItems.map((obj) => obj.item);

  return (
    <div className="item-list">
      {props.items.map((item) => {
        return (
          <ItemPreview
            item={item}
            key={item.id}
            inCart={
              props.cartItems.length === 0 ? false : itemFromCart.includes(item)
            }
            onClickAddToCart={props.onClickAddToCart}
            onClickRemoveFromCart={props.onClickRemoveFromCart}
          />
        );
      })}

      {/* <ListPagination
        pager={props.pager}
        size={props.size}
        currentPage={props.currentPage}
        count={props.itemsCount}
        tab={props.tab}
        user={props.currentUser}
        section={props.section}
      /> */}
    </div>
  );
};

export default ItemList;
