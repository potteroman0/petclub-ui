import React from "react";

import { connect } from "react-redux";
import { Link } from "react-router-dom";
import agent from "../../agent";

import ItemsList from "./ItemList";

import {
  ADD_ITEM_TO_CART,
  CHANGE_SHOP_TAB,
  REMOVE_ITEM,
} from "../../constants/actionTypes";

const NewItemTab = (props) => {
  if (props.user) {
    if (props.token && props.user.role === "ADMIN") {
      return (
        <li className="item-editor-button">
          <Link to="item-editor" className="nav-link">
            <i className="ion-compose"></i>&nbsp;New Item
          </Link>
        </li>
      );
    }
  }
  return null;
};

const AllItemsTab = (props) => {
  const clickHandler = (ev) => {
    ev.preventDefault();
    props.onTabClick(
      "all",
      agent.Items.getAllItems,
      agent.Items.getAllItems(props.size, props.page)
    );
  };
  return (
    <li className="nav-item">
      <a
        href=""
        className={props.tab === "all" ? "nav-link active" : "nav-link"}
        onClick={clickHandler}
      >
        Items
      </a>
    </li>
  );
};

const CategoryFilterTab = (props) => {
  if (!props.category) {
    return null;
  }

  return (
    <li className="nav-item">
      <a href="" className="nav-link active">
        <i className="ion-pound"></i> {props.category}
      </a>
    </li>
  );
};

const RecomendedItemsTab = (props) => {
  const clickHandler = (ev) => {
    ev.preventDefault();
    props.onTabClick(
      "recommended",
      agent.Items.getAllItems,
      agent.Items.getAllItems(3, props.page, "DESC", "createDate")
    );
  };
  if (props.user) {
    console.log(props.tab, props.tab === "recommended");
    return (
      <li className="nav-item">
        <a
          href=""
          className={
            props.tab === "recommended" ? "nav-link active" : "nav-link"
          }
          onClick={clickHandler}
        >
          Recomended
        </a>
      </li>
    );
  } else return null;
};

const mapDispatchToProps = (dispatch) => ({
  onClickAddToCart: (payload) => dispatch({ type: ADD_ITEM_TO_CART, payload }),
  onClickRemoveFromCart: (itemId) => dispatch({ type: REMOVE_ITEM, itemId }),
  onShopTabClick: (tab, pager, payload) =>
    dispatch({ type: CHANGE_SHOP_TAB, tab, pager, payload }),
});

const mapStateToProps = (state) => ({
  ...state.itemsList,
  cartItems: state.cart.itemAmount,
  token: state.common.token,
  size: state.common.size,
  user: state.common.currentUser,
});

const ItemsView = (props) => {
  console.log("tab", props.tab);
  return (
    <div className="item-page">
      <div className="col-md-9">
        <div className="feed-toggle">
          <ul className="nav nav-pills outline-active">
            <li className="Article_filter">
              <AllItemsTab
                tab={props.tab}
                size={props.size}
                page={0}
                onTabClick={props.onShopTabClick}
              />
              <NewItemTab token={props.token} user={props.user} />
              <RecomendedItemsTab
                tab={props.tab}
                user={props.user}
                size={props.size}
                page={0}
                onTabClick={props.onShopTabClick}
              />
              <CategoryFilterTab category={props.category} />
            </li>
          </ul>
        </div>
      </div>

      <ItemsList
        section={props.section}
        pager={props.pager}
        items={props.items}
        cartItems={props.cartItems}
        itemsCount={props.itemsCount}
        currentPage={props.currentPage}
        onClickAddToCart={props.onClickAddToCart}
        onClickRemoveFromCart={props.onClickRemoveFromCart}
      />
    </div>
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(ItemsView);
