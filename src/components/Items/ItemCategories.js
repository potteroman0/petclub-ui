import React from "react";
import agent from "../../agent";

const ItemCategories = (props) => {
  const categories = props.categories;
  if (categories) {
    return (
      <div className="tag-list">
        {categories.map((category) => {
          const handleClick = (ev) => {
            ev.preventDefault();
            props.onClickCategory(
              category,
              agent.Items.getItemByCategory,
              agent.Items.getItemByCategory(category, props.size, props.page)
            );
          };

          return (
            <a href="" className="tag-default tag-pill" onClick={handleClick}>
              {category}
            </a>
          );
        })}
      </div>
    );
  } else {
    return <div className="loading">Loading Categories...</div>;
  }
};

export default ItemCategories;
