import React from "react";

import { Link } from "react-router-dom";

const ItemPreview = (props) => {
  const item = props.item;

  const handleClick = (ev) => {
    if (props.inCart) {
      props.onClickRemoveFromCart(item.id);
    } else {
      props.onClickAddToCart(item);
    }
  };

  return (
    <div className="item-preview">
      <div className="item-meta">
        <div className="item-img-title">
          <div className="item-img">
            <img src={item.imgUrl}></img>
          </div>
          <Link to={`item/${item.id}`} className="preview-link">
            <p className="item-title">{item.title}</p>
          </Link>
        </div>
        <div className="item-price-info">
          <div className="item-price">{item.price + " $"}</div>
          <div className="item-buy-button">
            <button
              className="btn btn-sm btn-outline-primary"
              onClick={handleClick}
            >
              <i className="material-icons md-18">
                {props.inCart ? "done_all" : "shopping_cart"}
              </i>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ItemPreview;
