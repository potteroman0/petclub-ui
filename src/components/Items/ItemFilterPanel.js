import React from "react";

import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import agent from "../../agent";
import { connect } from "react-redux";
import { APPLY_ITEM_FILTER } from "../../constants/actionTypes";

const mapStateToProps = (state) => ({
  ...state.itemsList,
  tab: state.common.tab,
  size: state.common.size,
  page: state.common.page,
  user: state.common.currentUser,
});

const mapDispatchToProps = (dispatch) => ({
  onClickFilter: (tab, filter, payload) =>
    dispatch({ type: APPLY_ITEM_FILTER, tab, filter, payload }),
});

const ItemFilterPanel = (props) => {
  const handleOrderClick = (ev) => {
    ev.preventDefault();

    console.log(props.tab);
    if (props.tab == "all") {
      props.onClickFilter(
        props.tab,
        {
          field: props.field,
          direction: ev.target.value,
        },
        agent.Items.getAllItems(
          props.size,
          props.page,
          ev.target.value,
          props.field
        )
      );
    } else if (props.category) {
      props.onClickFilter(
        props.tab,
        {
          field: props.field,
          direction: ev.target.value,
        },
        agent.Items.getItemByCategory(
          props.category,
          props.size,
          props.page,
          ev.target.value,
          props.field
        )
      );
    }
  };

  const handleFieldClick = (ev) => {
    ev.preventDefault();
    if (props.tab == "all") {
      props.onClickFilter(
        props.tab,
        {
          field: ev.target.value,
          direction: props.direction,
        },
        agent.Items.getAllItems(
          props.size,
          props.page,
          props.direction,
          ev.target.value
        )
      );
    } else if (props.category) {
      props.onClickFilter(
        props.tab,
        {
          field: ev.target.value,
          direction: props.direction,
        },
        agent.Items.getItemByCategory(
          props.category,
          props.size,
          props.page,
          props.direction,
          ev.target.value
        )
      );
    }
  };

  return (
    <div className="filter-block">
      <p>Filter:</p>
      <FormControl component="fieldset" className="fieldset">
        <FormLabel component="legend" className="legend">
          Field
        </FormLabel>
        <RadioGroup
          className="radio-group"
          aria-label="field"
          name="field"
          value={props.field}
          onChange={handleFieldClick}
        >
          <FormControlLabel
            className="legend-label"
            value="createDate"
            control={<Radio />}
            label="Date"
          />
          <FormControlLabel
            value="price"
            className="legend-label"
            control={<Radio />}
            label="Price"
          />
        </RadioGroup>
      </FormControl>

      <FormControl component="fieldset" className="fieldset">
        <FormLabel component="legend" className="legend">
          Order
        </FormLabel>
        <RadioGroup
          aria-label="direction"
          className="radio-group"
          name="direction"
          value={props.direction}
          onChange={handleOrderClick}
        >
          <FormControlLabel
            value="ASC"
            control={<Radio />}
            label="ASC"
            className="legend-label"
          />
          <FormControlLabel
            value="DESC"
            control={<Radio />}
            label="DESC"
            className="legend-label"
          />
        </RadioGroup>
      </FormControl>
    </div>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemFilterPanel);
