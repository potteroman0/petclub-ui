import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import agent from "../../agent";
import { connect } from "react-redux";
import { faDownload } from "@fortawesome/free-solid-svg-icons";
import ListErrors from "../ListErrors";

import {
  UPDATE_FIELD_EDITOR,
  IMAGE_LOAD,
  QUESTION_EDITOR_LOADED,
  QUESTION_EDITOR_UNLOADED,
  QUESTION_SUBMITTED,
  QUESTION_IMAGE_FOLDER,
} from "../../constants/actionTypes";

const mapStateToProps = (state) => ({
  ...state.questionEditor,
  user: state.common.currentUser,
});

const mapDispatchToProps = (dispatch) => ({
  onLoad: (payload) => dispatch({ type: QUESTION_EDITOR_LOADED, payload }),
  onUnload: () => dispatch({ type: QUESTION_EDITOR_UNLOADED }),
  onSubmit: (payload) => dispatch({ type: QUESTION_SUBMITTED, payload }),
  onUpdateField: (key, value) =>
    dispatch({ type: UPDATE_FIELD_EDITOR, key, value }),
  onLoadImage: (payload) => dispatch({ type: IMAGE_LOAD, payload }),
});

class QuestionEditor extends React.Component {
  constructor() {
    super();

    const updateFieldEvent = (key) => (ev) => {
      if (key === "imgUrl") {
        this.props.onLoadImage(
          agent.Photos.upload(QUESTION_IMAGE_FOLDER, ev.target.files[0])
        );
      } else {
        this.props.onUpdateField(key, ev.target.value);
      }
    };

    this.changeTitle = updateFieldEvent("title");
    this.changeText = updateFieldEvent("text");
    this.changeImage = updateFieldEvent("imgUrl");
    this.changeCategory = updateFieldEvent("category");

    this.submitForm = (ev) => {
      ev.preventDefault();
      const question = {
        title: this.props.title,
        text: this.props.text,
        imgUrl: this.props.imgUrl,
        category: this.props.category,
        user: this.props.createdUser ? this.props.createdUser : this.props.user,
      };

      const id = this.props.id;
      const promise = this.props.questionId
        ? agent.Questions.update(
            this.props.questionId,
            question.title,
            question.text,
            question.imgUrl,
            question.category.toUpperCase(),
            question.user
          )
        : agent.Questions.create(
            question.title,
            question.text,
            question.imgUrl,
            question.category.toUpperCase(),
            question.user
          );

      this.props.onSubmit(promise);
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.match.params.id !== nextProps.match.params.id) {
      if (nextProps.match.params.id) {
        this.props.onUnload();
        return this.props.onLoad(
          agent.Questions.getById(this.props.match.params.id)
        );
      }
      this.props.onLoad(null);
    }
  }

  componentDidMount() {
    if (this.props.match.params.id) {
      return this.props.onLoad(
        Promise.all([
          agent.Questions.getAllCategories(),
          agent.Questions.getById(this.props.match.params.id),
        ])
      );
    }
    this.props.onLoad(Promise.all([agent.Questions.getAllCategories(), null]));
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    return (
      <div className="editor-page">
        <div className="container page">
          <ListErrors errors={this.props.errors}></ListErrors>

          <form>
            <fieldset>
              <div className="row">
                <fieldset className="form-group">
                  <input
                    className="form-control-title"
                    type="text"
                    placeholder="Question Title"
                    value={this.props.title}
                    onChange={this.changeTitle}
                  />
                </fieldset>
                <fieldset>
                  <label for="image" className="avatar-change">
                    <FontAwesomeIcon icon={faDownload} />
                    &nbsp;Insert image
                  </label>
                  <input
                    className="avatar-change-input"
                    type="file"
                    id="image"
                    accept="image/png, image/jpeg"
                    onChange={this.changeImage}
                  />
                </fieldset>
              </div>

              <fieldset className="form-group">
                <textarea
                  className="form-control"
                  required
                  rows="8"
                  placeholder="Write your question"
                  value={this.props.text}
                  onChange={this.changeText}
                ></textarea>
              </fieldset>

              <div className="row">
                <fieldset className="category-list">
                  <label className="categories" for="category">
                    Choose a question category:{" "}
                  </label>

                  <select
                    name="category"
                    id="category"
                    onChange={this.changeCategory}
                  >
                    <option>none</option>
                    {this.props.categories.map((category) => (
                      <option
                        value={category}
                        selected={
                          category.toUpperCase() === this.props.category
                        }
                      >
                        {category}
                      </option>
                    ))}
                  </select>
                </fieldset>

                <button
                  className="btn-move btn-publish"
                  type="button"
                  disabled={this.props.inProgress}
                  onClick={this.submitForm}
                >
                  Publish Question
                </button>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(QuestionEditor);
