import React from "react";

import QuestionPreview from "./QuestionPreview";
import ListPagination from "../ListPagination";

const QuestionList = (props) => {
  if (!props.questions) {
    return <div className="loading">Loading...</div>;
  }

  if (props.questions.length === 0) {
    return <div className="no-content">No questions are here... yet.</div>;
  }

  return (
    <div className="question-list">
      {props.questions.map((question) => {
        return <QuestionPreview question={question} key={question.id} />;
      })}

      <ListPagination
        pager={props.pager}
        size={props.size}
        category={props.category}
        count={props.questionsCount}
        currentPage={props.currentPage}
        section={props.section}
        tab={props.tab}
        user={props.user}
      />
    </div>
  );
};

export default QuestionList;
