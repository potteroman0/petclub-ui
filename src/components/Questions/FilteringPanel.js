import React from "react";

import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import agent from "../../agent";
import FormGroup from "@material-ui/core/FormGroup";
import { connect } from "react-redux";
import { APPLY_RESOLVED_FILTER } from "../../constants/actionTypes";

const mapStateToProps = (state) => ({
  ...state.questionList,
  size: state.common.size,
  page: state.common.page,
  user: state.common.currentUser,
});

const mapDispatchToProps = (dispatch) => ({
  onClickFilter: (payload) =>
    dispatch({ type: APPLY_RESOLVED_FILTER, payload }),
});

const FilteringPanel = (props) => {
  const [state, setState] = React.useState({
    resolved: false,
  });

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
    console.log(event.target.checked);
    if (event.target.checked) {
      props.onClickFilter(
        agent.Questions.getAllResolved(props.size, props.page)
      );
    } else {
      props.onClickFilter(
        agent.Questions.getAllPaginable(props.size, props.page)
      );
    }
  };

  return (
    <div>
      <FormGroup row>
        <FormControlLabel
          control={
            <Switch
              checked={state.resolved}
              onChange={handleChange}
              name="resolved"
            />
          }
          label="Resolved"
        />
      </FormGroup>

      {/* <FormControl component="fieldset">
        <FormLabel component="legend">Field</FormLabel>
        <RadioGroup
          aria-label="field"
          name="field"
          value={props.field}
          onChange={handleFieldClick}
        >
          <FormControlLabel
            value="createDate"
            control={<Radio />}
            label="Date"
          />
          <FormControlLabel value="rating" control={<Radio />} label="Rating" />
        </RadioGroup>
      </FormControl>

      <FormControl component="fieldset">
        <FormLabel component="legend">Order</FormLabel>
        <RadioGroup
          aria-label="direction"
          name="direction"
          value={props.direction}
          onChange={handleOrderClick}
        >
          <FormControlLabel value="ASC" control={<Radio />} label="ASC" />
          <FormControlLabel value="DESC" control={<Radio />} label="DESC" />
        </RadioGroup>
      </FormControl> */}
    </div>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(FilteringPanel);
