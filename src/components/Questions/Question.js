import React from "react";

import agent from "../../agent";
import { connect } from "react-redux";
import DOMPurify from "dompurify";
import { convertToHTML } from "draft-convert";
import { EditorState, convertFromRaw, convertToRaw } from "draft-js";

import QuestionMeta from "./QuestionMeta";
import CommentContainer from "../Comments/CommentContainer";
import {
  QUESTION_PAGE_LOADED,
  QUESTION_PAGE_UNLOADED,
} from "../../constants/actionTypes";

const mapStateToProps = (state) => ({
  ...state.question,
  currentUser: state.common.currentUser,
  section: state.question.section,
});

const mapDispatchToProps = (dispatch) => ({
  onLoad: (payload) => dispatch({ type: QUESTION_PAGE_LOADED, payload }),
  onUnload: () => dispatch({ type: QUESTION_PAGE_UNLOADED }),
});

class Question extends React.Component {
  componentDidMount() {
    this.props.onLoad(
      Promise.all([
        agent.Questions.getById(this.props.match.params.id),
        agent.Answers.getByQuestionId(this.props.match.params.id),
      ])
    );
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    if (!this.props.question) {
      return null;
    }

    const canModify =
      this.props.currentUser === null
        ? false
        : this.props.currentUser.username ===
            this.props.question.user.username ||
          this.props.currentUser.role === "ADMIN";
    return (
      <div className="article-page">
        <div className="banner">
          <div className="container">
            <h1 className="article_title title-article-page">
              {this.props.question.title}
            </h1>
            <QuestionMeta
              question={this.props.question}
              canModify={canModify}
            />
          </div>
        </div>

        {/* <div className="container-page">
          <div
            className="article-content"> {this.props.question.text}</div> */}
        <div className="container-page">
          <hr />

          <div className="article-actions">
            <div className="row">
              <CommentContainer
                showResolve={canModify}
                answers={this.props.answers || []}
                section={this.props.section}
                errors={[]}
                id={this.props.match.params.id}
                currentUser={this.props.currentUser}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Question);
