import React from "react";

import { connect } from "react-redux";
import agent from "../../agent";
import { Link } from "react-router-dom";

import { CHANGE_QUESTION_TAB } from "../../constants/actionTypes";
import QuestionsList from "../Questions/QuestionList";

const YourQuestionsTab = (props) => {
  if (props.token) {
    const page = 0;
    const clickHandler = (ev) => {
      ev.preventDefault();
      props.onTabClick(
        "my",
        agent.Questions.getByUserId,
        agent.Questions.getByUserId(props.user.id, props.size, props.page)
      );
    };

    return (
      <li className="nav-item">
        <a
          href=""
          className={props.tab === "my" ? "nav-link active" : "nav-link"}
          onClick={clickHandler}
        >
          My Questions
        </a>
      </li>
    );
  }
  return null;
};

const AllQuestionsTab = (props) => {
  const clickHandler = (ev) => {
    ev.preventDefault();
    props.onTabClick(
      "all",
      agent.Questions.getAllPaginable,
      agent.Questions.getAllPaginable(props.size, props.page)
    );
  };
  return (
    <li className="nav-item">
      <a
        href=""
        className={props.tab === "all" ? "nav-link active" : "nav-link"}
        onClick={clickHandler}
      >
        All Questions
      </a>
    </li>
  );
};

const NewQuestionTab = (props) => {
  if (props.token) {
    return (
      <li className="nav-item">
        <Link to="question-editor" className="nav-link">
          <i className="ion-compose"></i>&nbsp;New Question
        </Link>
      </li>
    );
  }
  return null;
};

const CategoryFilterTab = (props) => {
  if (!props.category) {
    return null;
  }

  return (
    <li className="nav-item">
      <a href="" className="nav-link active">
        <i className="ion-pound"></i> {props.category}
      </a>
    </li>
  );
};

const mapDispatchToProps = (dispatch) => ({
  onTabClick: (tab, pager, payload) =>
    dispatch({ type: CHANGE_QUESTION_TAB, tab, pager, payload }),
});

const mapStateToProps = (state) => ({
  ...state.questionList,
  token: state.common.token,
  size: state.common.size,
  user: state.common.currentUser,
});

const QuestionsView = (props) => {
  return (
    <div className="col-md-9">
      <div className="feed-toggle">
        <ul className="nav nav-pills outline-active">
          <li className="col-md-3">
            <YourQuestionsTab
              token={props.token}
              tab={props.tab}
              size={props.size}
              page={props.currentPage}
              user={props.user}
              onTabClick={props.onTabClick}
            />

            <AllQuestionsTab
              tab={props.tab}
              size={props.size}
              page={props.currentPage}
              onTabClick={props.onTabClick}
            />
          </li>

          <CategoryFilterTab category={props.category} />

          <NewQuestionTab token={props.token} />
        </ul>
      </div>

      <QuestionsList
        user={props.user}
        tab={props.tab}
        size={props.size}
        pager={props.pager}
        questions={props.questions}
        loading={props.loading}
        questionsCount={props.questionsCount}
        currentPage={props.currentPage}
        section={props.section}
        category={props.category}
      />
    </div>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(QuestionsView);
