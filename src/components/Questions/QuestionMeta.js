import React from "react";
import { Link } from "react-router-dom";
import QuestionActions from "./QuestionActions";

const QuestionMeta = (props) => {
  const question = props.question;
  return (
    <div className="username-info">
      <div className="article-meta">
        <div className="user-info">
          <div className="user-avatar-image">
            <Link to={`/@${question.user.username}`}>
              <img src={question.user.imgUrl} alt={question.user.username} />
            </Link>
          </div>

          <div className="info">
            <Link to={`/@${question.user.username}`} className="author">
              {question.user.username}
            </Link>
            <span className="date">
              {new Date(question.createDate).toDateString()}
            </span>
          </div>

          <QuestionActions canModify={props.canModify} question={question} />
        </div>
      </div>
    </div>
  );
};

export default QuestionMeta;
