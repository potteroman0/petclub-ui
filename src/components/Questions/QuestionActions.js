import React from "react";

import { Link } from "react-router-dom";
import agent from "../../agent";
import { connect } from "react-redux";

import {
  QUESTION_DELETED,
  RESOLVE_QUESTION,
} from "../../constants/actionTypes";

const mapDispatchToProps = (dispatch) => ({
  onClickDelete: (payload) => dispatch({ type: QUESTION_DELETED, payload }),
  onClickResolve: (payload) => dispatch({ type: RESOLVE_QUESTION, payload }),
});

const QuestionActions = (props) => {
  const question = props.question;
  const del = () => {
    props.onClickDelete(agent.Questions.delete(question.id));
  };

  const resolve = () => {
    props.onClickResolve(agent.Questions.resolve(question.id));
  };

  if (props.canModify) {
    return (
      <span>
        <Link
          to={`/question-editor/${question.id}`}
          className="btn-secondary btn-edit"
        >
          <i className="ion-edit"></i> Edit
        </Link>

        <button className="btn-secondary btn-del" onClick={del}>
          <i className="ion-trash-a"></i> Delete
        </button>
        <button className="btn-secondary btn-del" onClick={resolve}>
          <i className="material-icons md-18"> </i>
          {question.resolved ? "unresolved" : "resolved"}
        </button>
      </span>
    );
  }

  return <span></span>;
};

export default connect(() => ({}), mapDispatchToProps)(QuestionActions);
