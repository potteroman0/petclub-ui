import React from "react";

import agent from "../../agent";
import { connect } from "react-redux";

import Banner from "../Home/Banner";
import {
  APPLY_CATEGORY_FILTER,
  QUESTIONS_PAGE_LOADED,
  QUESTIONS_PAGE_UNLOADED,
} from "../../constants/actionTypes";
import QuestionsView from "./QuestionsView";
import Categories from "./Categories";
import FilteringPanel from "./FilteringPanel";

const Promise = global.Promise;

const mapStateToProps = (state) => ({
  appName: state.common.appName,
  size: state.common.size,
  page: state.common.page,
  tab: state.common.tab,
  questions: state.questionList.questions,
  answers: state.questionList.answers,
  categories: state.questionList.categories,
});

const mapDispatchToProps = (dispatch) => ({
  onLoad: (tab, pager, payload) =>
    dispatch({ type: QUESTIONS_PAGE_LOADED, tab, pager, payload }),
  onUnload: () => dispatch({ type: QUESTIONS_PAGE_UNLOADED }),
  onClickCategory: (category, pager, payload) =>
    dispatch({ type: APPLY_CATEGORY_FILTER, category, pager, payload }),
});

class Questions extends React.Component {
  componentDidMount() {
    const questionsPromise = agent.Questions.getAllPaginable;
    this.props.onLoad(
      this.props.tab,
      questionsPromise,
      Promise.all([
        agent.Questions.getAllCategories(),
        questionsPromise(this.props.size, this.props.page),
      ])
    );
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    return (
      <div className="question">
        <Banner appName={this.props.appName} />

        <div className="container page">
          <div className="row">
            <QuestionsView />

            <div className="sidebar">
              <div className="regular">
                <p className="categories">Categories</p>

                <Categories
                  categories={this.props.categories}
                  onClickCategory={this.props.onClickCategory}
                  size={this.props.size}
                  page={this.props.page}
                />
                <FilteringPanel />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Questions);
