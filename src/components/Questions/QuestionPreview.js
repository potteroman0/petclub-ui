import React from "react";
import { Link } from "react-router-dom";

const QuestionPreview = (props) => {
  const question = props.question;

  return (
    <div className="article-preview">
      <div className="article-meta">
        <div className="user-info">
          <div className="user-avatar-image">
            <Link to={`/${question.user.username}`}>
              <img src={question.user.imgUrl} alt={question.user.username} />
            </Link>
          </div>

          <div className="info">
            <Link className="author" to={`/${question.user.username}`}>
              {question.user.username}
            </Link>
            <span className="date">
              {new Date(question.createDate).toDateString()}
            </span>
          </div>
        </div>
        <div className="pull-xs-right">
          <div className="solution">
            <i className="ff"></i>{" "}
            {question.resolved ? "resolved" : "in progress"}
          </div>
        </div>
      </div>

      <h1 className="article_title">{question.title}</h1>
      <div className="preview">{question.text}</div>
      <Link to={`questions/${question.id}`} className="preview-link">
        <span className="article-read-more btn-move">Read more</span>
      </Link>
      <div className="tagsbar">
        <ul className="tag-list">
          <li className="tag-default tag-pill tag-outline">
            {question.category}
          </li>
        </ul>
      </div>
    </div>
  );
};

export default QuestionPreview;
