import React from "react";
import agent from "../../agent";
import { connect } from "react-redux";

import { ADD_COMMENT } from "../../constants/actionTypes";

const mapDispatchToProps = (dispatch) => ({
  onSubmit: (payload) => dispatch({ type: ADD_COMMENT, payload }),
});

class CommentInput extends React.Component {
  constructor() {
    super();
    this.state = {
      text: "",
    };

    this.setBody = (ev) => {
      this.setState({ text: ev.target.value });
    };

    this.createComment = (ev) => {
      ev.preventDefault();
      const payload = agent.Comments.create(
        this.props.id,
        this.props.currentUser.id,
        this.state.text,
        this.props.currentUser.username,
        this.props.currentUser.imgUrl
      );
      this.setState({ text: "" });
      this.props.onSubmit(payload);
    };
  }

  render() {
    return (
      <form className="card comment-form" onSubmit={this.createComment}>
        <div className="card-block">
          <textarea
            className="form-control"
            placeholder="Write a comment..."
            value={this.state.text}
            onChange={this.setBody}
            rows="3"
          ></textarea>
        </div>
        <div className="card-footer">
          <div className="comment-author-img">
            <img
              src={this.props.currentUser.imgUrl}
              alt={this.props.currentUser.username}
            />
          </div>
          <button className="btn-move btn" type="submit">
            Post Comment
          </button>
        </div>
      </form>
    );
  }
}

export default connect(() => ({}), mapDispatchToProps)(CommentInput);
