import React from "react";
import agent from "../../agent";
import { connect } from "react-redux";
import { DELETE_COMMENT, DELETE_ANSWER } from "../../constants/actionTypes";

const mapDispatchToProps = (dispatch) => ({
  onClick: (payload, commentId) =>
    dispatch({ type: DELETE_COMMENT, payload, commentId }),
  onClickAnswerDelete: (payload, answerId) =>
    dispatch({ type: DELETE_ANSWER, payload, answerId }),
});

const DeleteButton = (props) => {
  let payload;
  const del = () => {
    if (props.section === "articles") {
      payload = agent.Comments.delete(props.slug, props.commentId);
      props.onClick(payload, props.commentId);
    } else if (props.section === "questions") {
      payload = agent.Answers.delete(props.commentId);
      props.onClickAnswerDelete(payload, props.commentId);
    }
  };

  if (props.show) {
    return (
      <span className="mod-options">
        <i className="ion-trash-a" onClick={del}></i>
      </span>
    );
  }
  return null;
};

export default connect(() => ({}), mapDispatchToProps)(DeleteButton);
