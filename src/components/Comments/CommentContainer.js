import React from "react";
import { Link } from "react-router-dom";

import CommentList from "./CommentList";
import CommentInput from "./CommentInput";
import AnswerInput from "./AnswerInput";

const InputView = (props) => {
  if (props.section === "articles") {
    return <CommentInput id={props.id} currentUser={props.currentUser} />;
  } else if (props.section === "questions") {
    return <AnswerInput id={props.id} currentUser={props.currentUser} />;
  }
  return null;
};

const CommentContainer = (props) => {
  let data = [];
  if (props.section === "articles") {
    data = props.comments;
  } else if (props.section === "questions") {
    data = props.answers;
  }

  if (props.currentUser) {
    return (
      <div className="col-xs-12 col-md-8 offset-md-2">
        <div>
          <InputView
            section={props.section}
            id={props.id}
            currentUser={props.currentUser}
          />
        </div>

        <CommentList
          showResolve={props.showResolve}
          comments={data ? data : []}
          id={props.id}
          currentUser={props.currentUser}
          section={props.section}
        />
      </div>
    );
  } else {
    return (
      <div className="reg-notice">
        <p>
          <Link to="/login">Sign in</Link>
          &nbsp;or&nbsp;
          <Link to="/register">sign up</Link>
          &nbsp;to add comments on this article.
        </p>

        <CommentList
          comments={data ? data : []}
          id={props.id}
          currentUser={props.currentUser}
        />
      </div>
    );
  }
};

export default CommentContainer;
