import React from "react";

import Comment from "./Comment";

const CommentList = (props) => {
  return (
    <div className="comment-list">
      {props.comments.map((comment) => {
        return (
          <Comment
            showResolve={props.showResolve}
            comment={comment}
            currentUser={props.currentUser}
            id={props.id}
            key={comment.id}
            section={props.section}
          />
        );
      })}
    </div>
  );
};

export default CommentList;
