import React from "react";

import agent from "../../agent";
import { connect } from "react-redux";

import { ADD_ANSWER } from "../../constants/actionTypes";

const mapDispatchToProps = (dispatch) => ({
  onSubmit: (payload) => dispatch({ type: ADD_ANSWER, payload }),
});

class AnswerInput extends React.Component {
  constructor() {
    super();
    this.state = {
      text: "",
    };

    this.setBody = (ev) => {
      this.setState({ text: ev.target.value });
    };

    this.createComment = (ev) => {
      ev.preventDefault();
      const payload = agent.Answers.create(
        this.state.text,
        this.props.id,
        this.props.currentUser.id
      );

      this.setState({ text: "" });
      this.props.onSubmit(payload);
    };
  }

  render() {
    return (
      <form className="card comment-form" onSubmit={this.createComment}>
        <div className="card-block">
          <textarea
            className="form-control"
            placeholder="Write a comment..."
            value={this.state.text}
            onChange={this.setBody}
            rows="3"
          ></textarea>
        </div>
        <div className="card-footer">
          <div className="comment-author-img">
            <img
              src={this.props.currentUser.imgUrl}
              className="comment-author-img"
              alt={this.props.currentUser.username}
            />
          </div>
          <button className="btn-move btn" type="submit">
            Post Answer
          </button>
        </div>
      </form>
    );
  }
}

export default connect(() => ({}), mapDispatchToProps)(AnswerInput);
