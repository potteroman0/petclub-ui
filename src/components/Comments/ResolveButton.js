import React from "react";

import agent from "../../agent";
import { connect } from "react-redux";

import { MARK_ANSWER_AS_RIGHT } from "../../constants/actionTypes";

const mapDispatchToProps = (dispatch) => ({
  onClick: (payload, answerId) =>
    dispatch({ type: MARK_ANSWER_AS_RIGHT, payload, answerId }),
});

const ResolveButton = (props) => {
  console.log(props.show);
  const mark = () => {
    console.log("xfdsdsds", props.commentId);
    const payload = agent.Answers.markAsRight(props.commentId);
    props.onClick(payload, props.commentId);
  };

  if (props.show && props.section === "questions") {
    return (
      <span className="mod-options">
        <i className="material-icons md-18" onClick={mark}>
          done
        </i>
      </span>
    );
  }
  return null;
};

export default connect(() => ({}), mapDispatchToProps)(ResolveButton);
