import React from "react";

import { Link } from "react-router-dom";

import DeleteButton from "./DeleteButton";
import ResolveButton from "./ResolveButton";

const RightAnswerLabel = (props) => {
  console.log(props.comment.rightAnswer);
  if (props.section === "questions") {
    return (
      <div className="comment-author">
        <button className="btn btn-sm btn-primary" type="submit">
          {props.comment.rightAnswer ? "Right" : null}
        </button>
      </div>
    );
  } else return null;
};

const Comment = (props) => {
  const comment = props.comment;
  const show =
    props.currentUser === null
      ? false
      : props.currentUser.username === props.comment.user.username ||
        props.currentUser.role === "ADMIN";
  return (
    <div
      className={
        props.section === "questions"
          ? props.comment.rightAnswer
            ? "card active"
            : "card"
          : "card"
      }
    >
      <div className="card-block">
        <p className="card-text">{comment.text}</p>
      </div>

      <div className="card-footer">
        <div className="user-information">
          <Link to={`/${comment.user.username}`} className="user-avatar-image">
            <img
              src={comment.user.imgUrl}
              className="comment-author-img"
              alt={comment.user.username}
            />
          </Link>
          &nbsp;
          <Link to={`/${comment.user.username}`} className="comment-author">
            {comment.user.username}
          </Link>
          <span className="date-posted">
            {new Date(comment.createdDate).toDateString()}
          </span>
        </div>
        <div>
          {/* <RightAnswerLabel section={props.section} comment={comment} /> */}
          <div>
            <DeleteButton
              section={props.section}
              show={show}
              id={props.id}
              commentId={comment.id}
            />
            <ResolveButton
              section={props.section}
              show={props.showResolve ? props.showResolve : false}
              id={props.id}
              commentId={comment.id}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Comment;
