import React from "react";
import agent from "../../agent";

const Tags = (props) => {
  const tags = props.hashtags;
  if (tags) {
    return (
      <div className="tag-list">
        {tags.map((tag) => {
          const handleClick = (ev) => {
            ev.preventDefault();
            props.onClickTag(
              tag.name,
              (page) => agent.Articles.getByHashtag(tag.name, props.size, page),
              agent.Articles.getByHashtag(tag.name, props.size, props.page)
            );
          };

          return (
            
            <a
              href=""
              className="tag-default tag-pill"
              key={tag.id}
              onClick={handleClick}
            >
              {tag.name}
            </a>
          );
        })}
      </div>
    );
  } else {
    return <div className="Loading">Loading Tags...</div>;
  }
};

export default Tags;
