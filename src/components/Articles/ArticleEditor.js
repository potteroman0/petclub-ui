import React from "react";

import agent from "../../agent";
import { connect } from "react-redux";

import { Editor } from "react-draft-wysiwyg";

import { EditorState, convertFromRaw, convertToRaw } from "draft-js";
import { convertToHTML } from "draft-convert";
import he from "he";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

import {
  ADD_TAG,
  EDITOR_PAGE_LOADED,
  REMOVE_TAG,
  ARTICLE_SUBMITTED,
  EDITOR_PAGE_UNLOADED,
  UPDATE_FIELD_EDITOR,
  ARTICLE_IMAGE_FOLDER,
  IMAGE_LOAD,
} from "../../constants/actionTypes";

const mapStateToProps = (state) => ({
  ...state.articleEditor,
  user: state.common.currentUser,
});

const mapDispatchToProps = (dispatch) => ({
  onLoad: (payload) => dispatch({ type: EDITOR_PAGE_LOADED, payload }),
  onUnload: () => dispatch({ type: EDITOR_PAGE_UNLOADED }),
  onAddTag: () => dispatch({ type: ADD_TAG }),
  onRemoveTag: (tag) => dispatch({ type: REMOVE_TAG, tag }),
  onSubmit: (payload) => dispatch({ type: ARTICLE_SUBMITTED, payload }),
  onUpdateField: (key, value) =>
    dispatch({ type: UPDATE_FIELD_EDITOR, key, value }),
  onLoadImage: (payload) => dispatch({ type: IMAGE_LOAD, payload }),
});

class ArticleEditor extends React.Component {
  constructor() {
    super();
    this.state = {
      editorState: EditorState.createEmpty(),
      firstLoad: true,
    };

    const updateFieldEvent = (key) => (ev) => {
      this.props.onUpdateField(key, ev.target.value);
    };

    this.changeTagInput = updateFieldEvent("tagInput");
    this.changeTitle = updateFieldEvent("title");

    this.watchForEnter = (ev) => {
      if (ev.keyCode === 13) {
        ev.preventDefault();
        this.props.onAddTag();
      }
    };

    this.removeTagHandler = (tag) => () => {
      this.props.onRemoveTag(tag);
    };
  }

  uploadImageToArticle = (file) => {
    return new Promise((resolve, reject) => {
      let imagePromise = agent.Photos.upload(ARTICLE_IMAGE_FOLDER, file);
      imagePromise.then((resp) => {
        let response = {
          data: {
            link: resp.url,
          },
        };
        resolve(response);
      });
    });
  };

  setUpEditor = () => {
    const textToView = this.props.text;
    if (textToView) {
      const message = JSON.parse(he.decode(textToView));
      if (message.blocks[0].text === "") {
        const editorState = EditorState.createEmpty();
        this.setEditorState(editorState);
      } else {
        const contentState = convertFromRaw(message);
        const editorState = EditorState.createWithContent(contentState);
        this.setEditorState(editorState);
      }
    }
  };

  setEditorState = (editorState) => {
    this.setState({
      editorState,
      firstLoad: false,
    });
  };

  onEditorStateChange = (editorState) => {
    this.setEditorState(editorState);
  };

  submitForm = (ev) => {
    ev.preventDefault();
    let currentContentAsHTML = convertToHTML(
      this.state.editorState.getCurrentContent()
    );
    this.setConvertedContent(currentContentAsHTML);
  };

  setConvertedContent = (currentContentAsHTML) => {
    const textToSave = JSON.stringify(
      convertToRaw(this.state.editorState.getCurrentContent())
    );

    const article = {
      title: this.props.title,
      text: this.props.text,
      hashtags: this.props.tagList,
      user: this.props.user,
    };

    const promise = this.props.articleId
      ? agent.Articles.update(
          this.props.articleId,
          article.title,
          textToSave,
          article.hashtags,
          article.user
        )
      : agent.Articles.create(
          article.title,
          textToSave,
          article.hashtags,
          article.user
        );

    this.props.onSubmit(promise);
  };

  componentDidUpdate(prevProps) {
    if (this.props.text && this.state.firstLoad) {
      this.setUpEditor();
    }
  }

  componentDidMount() {
    if (this.props.match.params.id) {
      this.props.onLoad(agent.Articles.getById(this.props.match.params.id));
    }
    this.props.onLoad(null);
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    return (
      <div className="articleEditorPage">
        <div className="container">
          <div className="editor-page">
            <div className="articleEditor">
              <Editor
                editorState={this.state.editorState}
                toolbarClassName="toolbarClassName"
                wrapperClassName="wrapperClassName"
                editorClassName="editorClassName"
                onEditorStateChange={this.onEditorStateChange}
                toolbar={{
                  image: {
                    uploadCallback: this.uploadImageToArticle,
                    alt: { present: true, mandatory: true },
                    defaultSize: { height: 300, width: 400 },
                  },
                }}
              />
            </div>
            <fieldset className="article-title-element">
              <input
                className="article-title form-control"
                type="text"
                placeholder="Article Title"
                value={this.props.title}
                onChange={this.changeTitle}
              />
            </fieldset>
          </div>
          <hr />
          <div className="tag-and-send-field">
            <div className="send-button-element">
              <button
                className="btn-move btn"
                type="button"
                disabled={this.props.inProgress}
                onClick={this.submitForm}
              >
                Publish Article
              </button>
            </div>
            <div className="tags-input-element">
              <fieldset className="form-group">
                <input
                  className="form-control"
                  type="text"
                  placeholder="Enter tags..."
                  value={this.props.tagInput}
                  onChange={this.changeTagInput}
                  onKeyUp={this.watchForEnter}
                />

                <div className="tag-list">
                  {(this.props.tagList || []).map((tag) => {
                    return (
                      <span className="tag-default tag-pill" key={tag.id}>
                        <i
                          className="ion-close-round"
                          onClick={this.removeTagHandler(tag.name)}
                        ></i>
                        {tag.name ? tag.name : tag}
                      </span>
                    );
                  })}
                </div>
              </fieldset>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ArticleEditor);
