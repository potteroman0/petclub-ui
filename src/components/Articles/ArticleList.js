import React from "react";

import ArticlePreview from "./ArticlePreview";
import ListPagination from "../ListPagination";

const ArticleList = (props) => {
  if (!props.articles) {
    return <div className="loading-articles">Loading...</div>;
  }

  if (props.articles.length === 0) {
    return <div className="no-articles">No articles are here... yet.</div>;
  }

  return (
    <div>
      {props.articles.map((article) => {
        return <ArticlePreview article={article} key={article.id} />;
      })}

      <ListPagination
        pager={props.pager}
        size={props.size}
        currentPage={props.currentPage}
        count={props.articlesCount}
        tab={props.tab}
        hashtag={props.tag}
        user={props.currentUser}
        filter={props.filter ? props.filter : null}
        section={props.section}
      />
    </div>
  );
};

export default ArticleList;
