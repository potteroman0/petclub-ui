import React from "react";

import { Link } from "react-router-dom";
import DOMPurify from "dompurify";
import { convertToHTML } from "draft-convert";
import { convertFromRaw } from "draft-js";
import Rating from "@material-ui/lab/Rating";

const createMarkup = (text) => {
  let currentContentAsHTML = convertToHTML(convertFromRaw(JSON.parse(text)));
  return {
    __html: DOMPurify.sanitize(currentContentAsHTML),
  };
};

const ArticlePreview = (props) => {
  const article = props.article;

  return (
    <div className="article-preview">
      <div className="article-meta">
        <div className="user-info">
          <div className="user-avatar-image">
            <Link to={`/${article.user.username}`}>
              <img src={article.user.imgUrl} alt={article.user.username} />
            </Link>
          </div>

          <div className="info">
            <Link className="author" to={`/${article.user.username}`}>
              {article.user.username}
            </Link>
            <span className="date">
              {new Date(article.createDate).toDateString()}
            </span>
          </div>
        </div>

        <div className="pull-xs-right">
          <Rating
            name="half-rating-read"
            value={article.rating}
            precision={0.5}
            readOnly
          />
        </div>
      </div>

      <h1 className="article_title">{article.title}</h1>
      <div
        className="preview"
        dangerouslySetInnerHTML={
          article.text === null ? null : createMarkup(article.text)
        }
      ></div>
      <Link to={`article/${article.id}`} className="preview-link">
        <span className="article-read-more btn-move">Read more</span>
      </Link>
      <ul className="tag-list">
        {article.hashtags.map((tag) => {
          return (
            <li className="tag-default tag-pill tag-outline" key={tag.id}>
              <span>{tag.name}</span>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default ArticlePreview;
