import React from "react";

import agent from "../../agent";
import { connect } from "react-redux";

import Banner from "../Home/Banner";
import ArticlesView from "./ArticlesView";
import Tags from "../Hashtag/Tags";

import {
  ARTICLES_PAGE_LOADED,
  ARTICLES_PAGE_UNLOADED,
  APPLY_TAG_FILTER,
  CREATE_TAG,
  DELETE_TAG,
} from "../../constants/actionTypes";
import FilteringPanel from "./FilteringPanel";

const Promise = global.Promise;

const mapStateToProps = (state) => ({
  appName: state.common.appName,
  size: state.common.size,
  page: state.common.page,
  tab: state.common.tab,
  hashtags: state.articleList.hashtags,
  user: state.common.currentUser,
});

const mapDispatchToProps = (dispatch) => ({
  onLoad: (tab, pager, payload) =>
    dispatch({ type: ARTICLES_PAGE_LOADED, tab, pager, payload }),
  onUnload: () => dispatch({ type: ARTICLES_PAGE_UNLOADED }),
  onClickTag: (tag, pager, payload) =>
    dispatch({ type: APPLY_TAG_FILTER, tag, pager, payload }),
  onCreateTag: (payload) => dispatch({ type: CREATE_TAG, payload }),
  onDeleteTag: (tag, payload) => dispatch({ type: DELETE_TAG, tag, payload }),
});

class Articles extends React.Component {
  componentDidMount() {
    const articlesPromise = agent.Articles.allPaginable;
    this.props.onLoad(
      this.props.tab,
      articlesPromise,
      Promise.all([
        agent.Tags.getAll(),
        articlesPromise(this.props.size, this.props.page),
      ])
    );
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    return (
      <div className="home-page">
        <Banner appName={this.props.appName} />

        <div className="container page">
          <div className="article_mainpage">
            <div className="ArticlesView">
              <ArticlesView />
            </div>

            <div className="sidebar">
              <div className="regular">
                <div className="filter_panel">
                  <FilteringPanel />
                </div>

                <div className="tagsbar">
                  <p>Popular Tags</p>
                  <Tags
                    hashtags={this.props.hashtags}
                    size={this.props.size}
                    page={this.props.page}
                    onClickTag={this.props.onClickTag}
                    user={this.props.user}
                    onCreateTag={this.props.onCreateTag}
                    onDeleteTag={this.props.onDeleteTag}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Articles);
