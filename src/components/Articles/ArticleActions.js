import React from "react";

import { Link } from "react-router-dom";
import agent from "../../agent";
import { connect } from "react-redux";

import { DELETE_ARTICLE } from "../../constants/actionTypes";

const mapDispatchToProps = (dispatch) => ({
  onClickDelete: (payload) => dispatch({ type: DELETE_ARTICLE, payload }),
});

const ArticleActions = (props) => {
  const article = props.article;
  const del = () => {
    props.onClickDelete(agent.Articles.delete(article.id));
  };
  if (props.canModify) {
    return (
      <span>
        <Link to={`/editor/${article.id}`} className="btn-secondary btn-edit">
          <i className="ion-edit"></i> Edit
        </Link>

        <button className="btn-secondary btn-del" onClick={del}>
          <i className="ion-trash-a"></i> Delete
        </button>
      </span>
    );
  }

  return <span></span>;
};

export default connect(() => ({}), mapDispatchToProps)(ArticleActions);
