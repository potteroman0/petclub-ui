import React from "react";
import agent from "../../agent";
import { connect } from "react-redux";
import DOMPurify from "dompurify";
import { convertToHTML } from "draft-convert";
import { EditorState, convertFromRaw, convertToRaw } from "draft-js";
import ArticleMeta from "./ArticleMeta";
import CommentContainer from "../Comments/CommentContainer";
import {
  ARTICLE_PAGE_LOADED,
  ARTICLE_PAGE_UNLOADED,
  RATE_ARTICLE,
} from "../../constants/actionTypes";

const mapStateToProps = (state) => ({
  ...state.article,
  currentUser: state.common.currentUser,
  rating: state.article.rating,
  section: state.article.section,
});

const mapDispatchToProps = (dispatch) => ({
  onLoad: (payload) => dispatch({ type: ARTICLE_PAGE_LOADED, payload }),
  onUnload: () => dispatch({ type: ARTICLE_PAGE_UNLOADED }),
  onRate: (payload) => dispatch({ type: RATE_ARTICLE, payload }),
});

const createMarkup = (text) => {
  let currentContentAsHTML = convertToHTML(convertFromRaw(JSON.parse(text)));
  return {
    __html: DOMPurify.sanitize(currentContentAsHTML),
  };
};

class Article extends React.Component {
  componentDidMount() {
    this.props.onLoad(
      Promise.all([
        agent.Articles.getById(this.props.match.params.id),
        agent.Comments.getByArticleId(this.props.match.params.id),
      ])
    );
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    if (!this.props.article) {
      return null;
    }

    const canModify =
      this.props.currentUser === null
        ? false
        : this.props.currentUser.username ===
            this.props.article.user.username ||
          this.props.currentUser.role === "ADMIN";

    return (
      <div className="article-page">
        <div className="banner">
          <div className="container">
            <h1 className="article_title title-article-page">
              {this.props.article.title}
            </h1>
            <ArticleMeta
              rating={this.props.rating}
              article={this.props.article}
              canModify={canModify}
              onRate={this.props.onRate}
            />
          </div>
        </div>

        <div className="container-page">
          <div
            className="article-content"
            dangerouslySetInnerHTML={
              this.props.article.text === null
                ? null
                : createMarkup(this.props.article.text)
            }
          ></div>

          <div className="tag-list">
            <ul className="tag-list-page">
              {this.props.article.hashtags.map((tag) => {
                return (
                  <li key={tag.id}>
                    <span>{tag.name}</span>
                  </li>
                );
              })}
            </ul>
          </div>
          <hr />
          <div className="article-actions">
            <div className="row">
              <CommentContainer
                comments={this.props.comments || []}
                errors={this.props.commentErrors}
                id={this.props.match.params.id}
                currentUser={this.props.currentUser}
                section={this.props.section}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Article);
