import React from "react";

import { connect } from "react-redux";
import agent from "../../agent";
import { Link } from "react-router-dom";

import { CHANGE_TAB } from "../../constants/actionTypes";
import ArticleList from "./ArticleList";

const MyArticlesTab = (props) => {
  if (props.token) {
    const clickHandler = (ev) => {
      ev.preventDefault();
      props.onTabClick(
        "my",
        agent.Articles.getByUserId,
        agent.Articles.getByUserId(props.user.id, props.size, props.page)
      );
    };

    return (
      <li className="nav-item">
        <a
          href=""
          className={props.tab === "my" ? "nav-link active" : "nav-link"}
          onClick={clickHandler}
        >
          My Articles
        </a>
      </li>
    );
  }
  return null;
};

const AllArticlesTab = (props) => {
  const clickHandler = (ev) => {
    ev.preventDefault();
    props.onTabClick(
      "all",
      agent.Articles.allPaginable,
      agent.Articles.allPaginable(props.size, props.page)
    );
  };
  return (
    <li className="nav-item">
      <a
        href=""
        className={props.tab === "all" ? "nav-link active" : "nav-link"}
        onClick={clickHandler}
      >
        All Articles
      </a>
    </li>
  );
};

const NewArticleTab = (props) => {
  if (props.token) {
    return (
      <li className="nav-item">
        <Link to="editor" className="nav-link">
          <i className="ion-compose"></i>&nbsp;New Post
        </Link>
      </li>
    );
  }
  return null;
};

const TagFilterTab = (props) => {
  if (!props.tag) {
    return null;
  }

  return (
    <li className="nav-item">
      <a href="" className="nav-link active">
        <i className="ion-pound"></i> {props.tag}
      </a>
    </li>
  );
};

const mapDispatchToProps = (dispatch) => ({
  onTabClick: (tab, pager, payload) =>
    dispatch({ type: CHANGE_TAB, tab, pager, payload }),
});

const mapStateToProps = (state) => ({
  ...state.articleList,
  token: state.common.token,
  size: state.common.size,
  user: state.common.currentUser,
  size: state.common.size,
});

const ArticlesView = (props) => {
  const filter = { field: props.field, direction: props.direction };
  return (
    <div className="col-md-9">
      <div className="feed-toggle">
        <ul className="nav nav-pills outline-active">
          <li className="Article_filter">
            <MyArticlesTab
              token={props.token}
              tab={props.tab}
              size={props.size}
              page={props.currentPage}
              user={props.user}
              onTabClick={props.onTabClick}
            />

            <AllArticlesTab
              tab={props.tab}
              size={props.size}
              page={props.currentPage}
              onTabClick={props.onTabClick}
            />
          </li>

          <TagFilterTab tag={props.tag} />

          <NewArticleTab token={props.token} />
        </ul>
      </div>

      <ArticleList
        pager={props.pager}
        size={props.size}
        currentPage={props.currentPage}
        articles={props.articles}
        articlesCount={props.articlesCount}
        loading={props.loading}
        currentUser={props.user}
        tab={props.tab}
        tag={props.tag}
        filter={filter}
        section={props.section}
      />
    </div>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(ArticlesView);
