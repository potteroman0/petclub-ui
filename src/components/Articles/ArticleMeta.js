import React from "react";
import { Link } from "react-router-dom";
import Rating from "@material-ui/lab/Rating";
import agent from "../../agent";
import ArticleActions from "./ArticleActions";

const ArticleMeta = (props) => {
  const article = props.article;
  return (
    <div className="username-info">
      <div className="article-meta">
        <div className="user-info">
          <div className="user-avatar-image">
            <Link to={`/${article.user.username}`}>
              <img src={article.user.imgUrl} alt={article.user.username} />
            </Link>
          </div>

          <div className="info">
            <Link to={`/${article.user.username}`} className="author">
              {article.user.username}
            </Link>
            <span className="date">
              {new Date(article.createDate).toDateString()}
            </span>
          </div>

          <ArticleActions canModify={props.canModify} article={article} />
        </div>

        <div className="pull-xs-right">
          <Rating
            name="half-rating-read"
            value={props.rating}
            precision={0.5}
            readOnly={props.canModify}
            onChange={(event, newValue) => {
              props.onRate(
                agent.Articles.rateAricle(article.id, article.user.id, newValue)
              );
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default ArticleMeta;
