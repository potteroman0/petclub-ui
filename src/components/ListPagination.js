import React from "react";

import agent from "../agent";
import { connect } from "react-redux";

import {
  SET_ARTICLE_PAGE,
  SET_QUESTION_PAGE,
  SET_ITEM_PAGE,
} from "../constants/actionTypes";

const mapDispatchToProps = (dispatch) => ({
  onSetArticlePage: (page, filter, payload) =>
    dispatch({ type: SET_ARTICLE_PAGE, page, filter, payload }),
  onSetQuestionPage: (page, filter, payload) =>
    dispatch({ type: SET_QUESTION_PAGE, page, filter, payload }),
  onSetItemPage: (page, payload) =>
    dispatch({ type: SET_ITEM_PAGE, page, payload }),
});

const ListPagination = (props) => {
  if (props.count <= props.size) {
    return null;
  }
  const range = [];
  for (let i = 0; i < Math.ceil(props.count / props.size); ++i) {
    range.push(i);
  }

  const setPage = (page) => {
    if (props.pager) {
      if (props.section === "questions") {
        if (props.tab === "all") {
          props.onSetQuestionPage(
            page,
            props.filter,
            props.pager(props.size, page)
          );
        } else if (props.tab === "my" && props.user) {
          props.onSetQuestionPage(
            page,
            props.filter,
            props.pager(props.user.id, props.size, page)
          );
        } else if (props.category) {
          props.onSetQuestionPage(
            page,
            props.filter,
            props.pager(props.category, props.size, page)
          );
        }
      } else if (props.section === "articles") {
        if (props.tab === "all") {
          props.onSetArticlePage(
            page,
            props.filter,
            props.pager(
              props.size,
              page,
              props.filter.direction,
              props.filter.field
            )
          );
        } else if (props.tab === "my" && props.user) {
          props.onSetArticlePage(
            page,
            props.filter,
            props.pager(
              props.user.id,
              props.size,
              page,
              props.filter.direction,
              props.filter.field
            )
          );
        } else if (props.hashtag) {
          props.onSetArticlePage(
            page,
            props.filter,
            props.pager(
              props.hashtag,
              props.size,
              page,
              props.filter.direction,
              props.filter.field
            )
          );
        }
      } else if (props.section === "items") {
        if (props.tab === "all") {
          props.onSetItemPage(page, props.pager(props.size, page));
        }
      }
    }

    //}
    // else {
    //   props.onSetPage(page, agent.Articles.allPaginable(props.total, page));
    // }
  };

  return (
    <nav>
      <ul className="pagination">
        {range.map((v) => {
          const isCurrent = v === props.currentPage;
          const onClick = (ev) => {
            ev.preventDefault();
            setPage(v);
          };
          return (
            <li
              className={isCurrent ? "page-item active" : "page-item"}
              onClick={onClick}
              key={v.toString()}
            >
              <a className="page-link" href="">
                {v + 1}
              </a>
            </li>
          );
        })}
      </ul>
    </nav>
  );
};

export default connect(() => ({}), mapDispatchToProps)(ListPagination);
