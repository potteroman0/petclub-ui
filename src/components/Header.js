import React from "react";
import logo from "../ico/home_ico.svg";
import { Link } from "react-router-dom";

const ResourceNavigationLinks = (props) => {
  return (
    <div>
      <ul className="nav navbar-nav pull-xs-left">
        <li className="nav-item">
          <Link to="/articles" className="nav-link">
            Articles
          </Link>
        </li>
        <li className="nav-item">
          <Link to="/questions" className="nav-link">
            Questions
          </Link>
        </li>
        {/* <li className="nav-item">
          <Link to="/advertisements" className="nav-link">
            Advertisements
          </Link>
        </li> */}
        <li className="nav-item">
          <Link to="/shop" className="nav-link">
            Shop
          </Link>
        </li>
      </ul>
    </div>
  );
};

const LoggedOutView = (props) => {
  if (!props.currentUser) {
    return (
      <div className="header_navigation">
        <div className="navigationlink">
          <ResourceNavigationLinks />
        </div>
        <div className="sitelink">
          <ul className="nav navbar-nav pull-xs-right">
            <li className="nav-item">
              <Link to="/login" className="nav-link">
                Sign in
              </Link>
            </li>

            <li className="nav-item">
              <Link to="/register" className="nav-link">
                Sign up
              </Link>
            </li>
          </ul>
        </div>
      </div>
    );
  }
  return null;
};

const LoggedInView = (props) => {
  if (props.currentUser) {
    return (
      <div className="header_navigation">
        <div className="navigationlink">
          <ResourceNavigationLinks />
        </div>
        <div className="sitelink">
          <ul className="nav navbar-nav pull-xs-right">
            <li className="nav-item">
              <Link to="/settings" className="nav-link">
                <i className="ion-gear-a"></i>&nbsp;
              </Link>
            </li>

            <li className="nav-item">
              <Link to={`${props.currentUser.username}`} className="nav-link">
                <div className="user-avatar-image">
                  <img src={props.currentUser.imgUrl} className="user-pic" />
                  {props.currentUser.username}
                </div>
              </Link>
            </li>

            <li className="nav-item">
              <Link to="/cart" className="nav-link">
                <i className="material-icons md-24">shopping_cart</i>&nbsp;
              </Link>
            </li>
          </ul>
        </div>
      </div>
    );
  }
  return null;
};

class Header extends React.Component {
  render() {
    return (
      <nav className="navbar navbar-light">
        <div className="container header_contain">
          <Link to="/" className="navbar-brand">
            <div className="brand-image">
              <img src={logo} alt="" />
            </div>
          </Link>

          <LoggedOutView currentUser={this.props.currentUser} />

          <LoggedInView currentUser={this.props.currentUser} />
        </div>
      </nav>
    );
  }
}

export default Header;
