import React from "react";

import { connect } from "react-redux";
import agent from "../../agent";

import Banner from "../Home/Banner";
import AdvertisementList from "./AdvertisementList";

import {
  ADVERTISIMENTS_PAGE_LOADED,
  ADVERTISIMENTS_PAGE_UNLOADED,
} from "../../constants/actionTypes";

const mapStateToProps = (state) => ({
  appName: state.common.appName,
  size: state.common.size,
  page: state.common.page,
});

const mapDispatchToProps = (dispatch) => ({
  onLoad: (payload) => dispatch({ type: ADVERTISIMENTS_PAGE_LOADED, payload }),
  onunload: () => dispatch({ type: ADVERTISIMENTS_PAGE_UNLOADED }),
});

class Advertisements extends React.Component {
  componentDidMount() {
    this.props.onLoad(
      agent.Advertisements.getAllPaginable(this.props.size, this.props.page)
    );
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    return (
      <div className="home-page">
        <Banner appName={this.props.appName} />
        <div className="container page">
          <div className="row">
            <AdvertisementList />
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Advertisements);
