import React from "react";

import { connect } from "react-redux";
import AdvertisementList from "./AdvertisementList";

const allAdvertisementsTab = (props) => {
  const clickHandler = (ev) => {
    ev.preventDefault();
  };

  return (
    <li className="nav-item">
      <a
        href=""
        className={props.tab === "all" ? "nav-link active" : "nav-link"}
        onClick={clickHandler}
      >
        All Advertisements
      </a>
    </li>
  );
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({});

const AdvertisementsView = (props) => {
  return (
    <div className="col-md-9">
      <div className="feed-toggle">
        <ul className="nav nav-pills outline-active"></ul>
        <AdvertisementList advertisements={props.advertisements} />
      </div>
    </div>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(AdvertisementsView);
