import React from "react";

import { Link } from "react-router-dom";

const AdvertisementPreview = (props) => {
  const advertisement = props.advertisement;

  return (
    <div className="article-preview">
      <div className="article-meta">
        <Link to={`/@${advertisement.user.username}`}>
          <img
            src={advertisement.user.imgUrl}
            alt={advertisement.user.username}
          />
        </Link>

        <div className="info">
          <Link className="author" to={`/${advertisement.user.username}`}>
            {advertisement.user.username}
          </Link>
          <span className="date">
            {new Date(advertisement.createDate).toDateString()}
          </span>
        </div>
      </div>
      <Link to={`article/${advertisement.id}`} className="preview-link">
        <h1>{advertisement.title}</h1>
        <span>Read more...</span>
      </Link>
    </div>
  );
};

export default AdvertisementPreview;
