import React from "react";

import AdvertisementPreview from "./AdvertisementPreview";

const AdvertisementList = (props) => {
  if (!props.advertisements) {
    return <div className="article-preview">Loading...</div>;
  }

  if (props.advertisements.length === 0) {
    return <div className="article-preview">No articles are here... yet.</div>;
  }

  return (
    <div>
      {props.advertisements.map((advert) => {
        return <AdvertisementPreview advertisement={advert} key={advert.id} />;
      })}
    </div>
  );
};

export default AdvertisementList;
