import React from "react";

class ListErrors extends React.Component {
  render() {
    const errors = this.props.errors;
    console.log("error!!!", this.props);

    if (errors) {
      const errorArray = Object.keys(errors);
      return (
        <ul className="error-messages">
          <li>
            {"error:"} {errors["text"]}
          </li>
        </ul>
      );
    } else {
      return null;
    }
  }
}

export default ListErrors;
