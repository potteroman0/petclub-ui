import React from "react";

import { Link } from "react-router-dom";

const CartItem = (props) => {
  const itemObj = props.itemObj;

  const [amount, setAmount] = React.useState(props.itemObj.amount);

  const onChangeAmount = (ev) => {
    setAmount(ev.target.value);
    if (ev.target.value !== "") {
      props.onChangeAmount(
        itemObj.item,
        ev.target.value * itemObj.item.price,
        ev.target.value
      );
    }
  };

  const onRemoveItem = (ev) => {
    props.onRemoveItem(itemObj.item.id);
  };

  return (
    <div className="basket-product">
      <div className="item">
        <div className="product-image">
          <img src={itemObj.item.imgUrl} alt="" className="product-frame" />
        </div>
        <div className="product-details">
          <h1 className="h1-cart">{itemObj.item.title}</h1>
          <p className="p-cart">{itemObj.item.description}</p>
        </div>
      </div>
      <div className="price">{itemObj.item.price}</div>
      <div className="quantity">
        <input
          type="number"
          value={amount}
          min="1"
          class="quantity-field"
          onChange={onChangeAmount}
        />
      </div>
      <div className="subtotal">{itemObj.totalPrice}</div>
      <div className="remove">
        <button onClick={onRemoveItem}>Remove</button>
      </div>
    </div>
  );
};

export default CartItem;
