import React from "react";
import GooglePayButton from "@google-pay/button-react";

const PayButton = (props) => {
  return (
    <div className="paypal-button">
      <GooglePayButton
        environment="TEST"
        paymentRequest={{
          apiVersion: 2,
          apiVersionMinor: 0,
          allowedPaymentMethods: [
            {
              type: "CARD",
              parameters: {
                allowedCardNetworks: ["VISA", "MASTERCARD"],
                allowedAuthMethods: ["PAN_ONLY", "CRYPTOGRAM_3DS"],
              },
              tokenizationSpecification: {
                type: "PAYMENT_GATEWAY",
                parameters: {
                  gateway: "example",
                },
              },
            },
          ],
          merchantInfo: {
            merchantId: "12345678901234567890",
            merchantName: "Demo Merchant",
          },
        }}
        onLoadPaymentData={(paymentData) => {
          console.log("Succes", paymentData);
          return { transactionState: "SUCCES" };
        }}
      />
    </div>
  );
};

export default PayButton;
