import React from "react";

import agent from "../../agent";
import { connect } from "react-redux";
import CartItemList from "./CartItemList";
import { CHANGE_AMOUNT, REMOVE_ITEM } from "../../constants/actionTypes";
import PayButton from "./PayButton";
import PaypalButton from "./PaypalButton";

const mapStateToProps = (state) => ({
  ...state.cart,
});

const mapDispatchToProps = (dispatch) => ({
  onChangeAmount: (item, price, amount) =>
    dispatch({ type: CHANGE_AMOUNT, item, price, amount }),
  onRemoveItem: (itemId) => dispatch({ type: REMOVE_ITEM, itemId }),
});

class Cart extends React.Component {
  render() {
    return (
      <div>
        <CartItemList
          items={this.props.itemAmount}
          onChangeAmount={this.props.onChangeAmount}
          onRemoveItem={this.props.onRemoveItem}
          itemsPrice={this.props.itemAmount}
        />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
