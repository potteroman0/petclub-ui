import React from "react";
import ItemPreview from "../Items/ItemPreview";

import CartItem from "./CartItem";
import PayButton from "./PayButton";
import PaypalButton from "./PaypalButton";

const CartItemList = (props) => {
  const [devPrice, setDevPrice] = React.useState("0");
  if (!props.items) {
    return <div className="article-preview">Loading...</div>;
  }

  if (props.items.length === 0) {
    return <div className="article-preview">No items are here... yet.</div>;
  }

  const onClickDelivery = (ev) => {
    console.log(ev.target.value);
    setDevPrice(ev.target.value);
  };

  const sumTotal = (arr) =>
    arr.reduce((sum, { totalPrice }) => sum + totalPrice, 0);

  return (
    <div>
      <div className="basket">
        <div class="basket-labels">
          <ul>
            <li class="item item-heading">Item</li>
            <li class="price">Price</li>
            <li class="quantity">Quantity</li>
            <li class="subtotal">Subtotal</li>
          </ul>
        </div>

        {props.items.map((itemObj) => {
          return (
            <CartItem
              itemObj={itemObj}
              key={itemObj.item.id}
              onChangeAmount={props.onChangeAmount}
              onRemoveItem={props.onRemoveItem}
            />
          );
        })}
      </div>

      <aside>
        <div className="summary">
          <div className="summary-total-items">
            <span className="total-items"></span> Items in your Bag
          </div>
          <div className="summary-subtotal">
            <div className="subtotal-title">Subtotal</div>
            <div className="subtotal-value final-value" id="basket-subtotal">
              {sumTotal(props.itemsPrice)}
            </div>
            <div className="summary-promo hide">
              <div className="promo-title">Promotion</div>
              <div className="promo-value final-value" id="basket-promo"></div>
            </div>
          </div>
          <div className="summary-delivery">
            <select
              name="delivery-collection"
              class="summary-delivery-selection"
              onChange={onClickDelivery}
            >
              <option value="0" selected="selected">
                Select Collection or Delivery
              </option>
              <option value={52}>NovaPoshta(50)</option>
              <option value={20}>UkrPoshta(20)</option>
              <option value={0}>Selfdelivery(free)</option>
            </select>
          </div>
          <div className="summary-total">
            <div className="total-title">Total</div>
            <div className="total-value final-value" id="basket-total">
              {sumTotal(props.itemsPrice) + parseInt(devPrice)}
            </div>
          </div>
          <div className="summary-checkout">
            <PayButton />
            <PaypalButton />
          </div>
        </div>
      </aside>
    </div>
  );
};

export default CartItemList;
