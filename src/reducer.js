import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import auth from "./reducers/auth";
import common from "./reducers/common";
import home from "./reducers/home";
import article from "./reducers/articles/article";
import articleList from "./reducers/articles/articleList";
import articleEditor from "./reducers/articles/articleEditor";
import settings from "./reducers/user/settings";
import questionEditor from "./reducers/questions/questionEditor";
import questionList from "./reducers/questions/questionList";
import question from "./reducers/questions/question";
import profile from "./reducers/user/profile";
import advertisementsList from "./reducers/advertisements/advertisementsList";
import itemsList from "./reducers/items/itemsList";
import item from "./reducers/items/item";
import itemEditor from "./reducers/items/itemEditor";
import cart from "./reducers/items/cart";

const createReducers = (history) =>
  combineReducers({
    auth,
    common,
    home,
    article,
    articleList,
    articleEditor,
    settings,
    questionEditor,
    questionList,
    question,
    profile,
    advertisementsList,
    item,
    itemsList,
    itemEditor,
    cart,
    router: connectRouter(history),
  });

export default createReducers;
