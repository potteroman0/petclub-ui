import {
  APP_LOAD,
  REDIRECT,
  LOGOUT,
  ARTICLE_SUBMITTED,
  SETTINGS_SAVED,
  LOGIN,
  REGISTER,
  DELETE_ARTICLE,
  CHANGE_TAB,
  CHANGE_SECTION,
  QUESTION_SUBMITTED,
  DELETE_USER_ACCOUNT,
  ITEM_SAVED,
  DELETE_ITEM,
  QUESTION_DELETED,
} from "../constants/actionTypes";

const defaultState = {
  appName: "PetClub",
  token: null,
  size: 3,
  section: "",
  tab: "all",
  page: 0,
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case APP_LOAD:
      return {
        ...state,
        token: action.token ? action.token : null,
        appLoaded: true,
        currentUser: action.payload ? action.payload.user : null,
      };
    case LOGIN:
      return {
        ...state,
        redirectTo: action.error ? null : "/",
        token: action.error ? null : action.payload.user.token,
        currentUser: action.error ? null : action.payload.user.user,
      };
    case REGISTER:
      return {
        ...state,
        redirectTo: action.error ? null : "/",
      };
    case LOGOUT:
    case DELETE_USER_ACCOUNT:
      return { ...state, redirectTo: "/", token: null, currentUser: null };
    case REDIRECT:
      return { ...state, redirectTo: null };
    case CHANGE_SECTION:
      return {
        ...state,
        section: action.tab,
      };

    case ITEM_SAVED:
      const redirectItemUrl = `/item/${action.payload.item.id}`;
      return { ...state, redirectTo: redirectItemUrl };
    case DELETE_ITEM:
      return { ...state, redirectTo: "/shop" };

    case ARTICLE_SUBMITTED:
      const redirectUrl = `/article/${action.payload.article.id}`;
      return { ...state, redirectTo: redirectUrl };
    case DELETE_ARTICLE:
      return { ...state, redirectTo: "/articles" };

    case QUESTION_SUBMITTED:
      const redirectUrl2 = `/questions/${action.payload.question.id}`;
      return { ...state, redirectTo: redirectUrl2 };
    case QUESTION_DELETED:
      return { ...state, redirectTo: "/questions" };

    case SETTINGS_SAVED:
      return {
        ...state,
        redirectTo: action.error ? null : "/",
        currentUser: action.error ? null : action.payload.user,
      };
    // case IMAGE_LOAD:
    //   return {
    //     ...state,
    //     imgUrl: action.payload ? action.payload.url : "",
    //   };
    // case UPDATE_FIELD_SETTINGS:
    //   return { ...state, [action.key]: action.value };
    // case SETTINGS_PAGE_LOADED:
    //   return {
    //     ...state,
    //     username: action.payload.username,
    //     imgUrl: action.payload.imgUrl,
    //     phone: action.payload.phone,
    //     email: action.payload.email,
    //     password: action.payload.password,
    //   };
    case CHANGE_TAB:
      return {
        ...state,
        pager: action.pager,
        articles: action.payload ? action.payload.articles.data : [],
        articlesCount: action.payload
          ? action.payload.articles.totalElements
          : 0,
        tab: action.tab,
        currentPage: 0,
        tag: null,
      };
  }

  return state;
};
