import {
  SETTINGS_SAVED,
  UPDATE_FIELD_SETTINGS,
  SETTINGS_PAGE_LOADED,
  SETTINGS_PAGE_UNLOADED,
  USER_IMAGE_LOAD,
} from "../../constants/actionTypes";

export default (state = {}, action) => {
  switch (action.type) {
    case SETTINGS_PAGE_LOADED:
      return {
        ...state,
        username: action.payload.username,
        imgUrl: action.payload.imgUrl,
        phone: action.payload.phone,
        email: action.payload.email,
        password: action.payload.password,
      };
    case SETTINGS_PAGE_UNLOADED:
      return { ...state };
    case UPDATE_FIELD_SETTINGS:
      return { ...state, [action.key]: action.value };
    case SETTINGS_SAVED:
      return {
        ...state,
        redirectTo: action.error ? null : "/",
        currentUser: action.error ? null : action.payload.user,
      };
    case USER_IMAGE_LOAD:
      console.log("ii");
      return {
        ...state,
        imgUrl: action.payload ? action.payload.url : "",
      };
  }
  return state;
};
