import {
  CHANGE_PROFILE_TAB,
  LOAD_RESOURCES,
  PROFILE_PAGE_LOADED,
  PROFILE_PAGE_UNLOADED,
} from "../../constants/actionTypes";

const defaultState = {
  articles: [],
  questions: [],
  orders: [],
  tab: "articles",
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case PROFILE_PAGE_LOADED:
      return {
        ...state,
        user: action.payload.user,
      };
    case PROFILE_PAGE_UNLOADED:
      return {};
    case LOAD_RESOURCES:
      return {
        ...state,
        articles: action.payload[0] ? action.payload[0].articles.data : [],
        questions: action.payload[1] ? action.payload[1].questions.data : [],
        orders: action.payload[2] ? action.payload[2].orders.data : [],
      };
    case CHANGE_PROFILE_TAB:
      return {
        ...state,
        tab: action.tab,
      };
    default:
      return state;
  }
};
