import {
  QUESTIONS_PAGE_LOADED,
  QUESTIONS_PAGE_UNLOADED,
  CHANGE_QUESTION_TAB,
  APPLY_CATEGORY_FILTER,
  SET_QUESTION_PAGE,
  APPLY_RESOLVED_FILTER,
} from "../../constants/actionTypes";

const defaultState = {
  categories: [],
  section: "questions",
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case QUESTIONS_PAGE_LOADED:
      return {
        ...state,
        pager: action.pager,
        categories: action.payload[0] ? action.payload[0].categories : [],
        questions: action.payload[1] ? action.payload[1].questions.data : [],
        answers: action.payload[1]
          ? action.payload[1].questions.questionAnswers
          : [],
        questionsCount: action.payload[1]
          ? action.payload[1].questions.totalElements
          : 0,
        currentPage: 0,
        tab: action.tab,
      };
    case QUESTIONS_PAGE_UNLOADED:
      return {};
    case CHANGE_QUESTION_TAB:
      return {
        ...state,
        pager: action.pager,
        questions: action.payload.questions.data,
        answers: action.payload.questions.questionAnswers,
        questionsCount: action.payload.questions.totalElements,
        tab: action.tab,
        currentPage: 0,
        category: null,
      };
    case APPLY_CATEGORY_FILTER:
      return {
        ...state,
        pager: action.pager,
        questions: action.payload.questions.data,
        questionsCount: action.payload.questions.totalElements,
        tab: null,
        category: action.category,
        currentPage: 0,
      };
    case APPLY_RESOLVED_FILTER:
      return {
        ...state,
        questions: action.payload ? action.payload.questions.data : [],
        questionsCount: action.payload
          ? action.payload.questions.totalElements
          : [],
      };
    case SET_QUESTION_PAGE:
      return {
        ...state,
        questions: action.payload.questions.data,
        answers: action.payload.questions.questionAnswers,
        questionsCount: action.payload.questions.totalElements,
        currentPage: action.page,
      };
    default:
      return state;
  }
};
