import {
  QUESTION_EDITOR_LOADED,
  QUESTION_EDITOR_UNLOADED,
  QUESTION_SUBMITTED,
  IMAGE_LOAD,
  UPDATE_FIELD_EDITOR,
  ASYNC_START,
} from "../../constants/actionTypes";

const defaultState = {
  categories: [],
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case QUESTION_EDITOR_LOADED:
      return {
        ...state,
        categories: action.payload[0].categories,
        questionId: action.payload[1] ? action.payload[1].question.id : "",
        title: action.payload[1] ? action.payload[1].question.title : "",
        text: action.payload[1] ? action.payload[1].question.text : "",
        imgUrl: action.payload[1] ? action.payload[1].question.imgUrl : "",
        category: action.payload[1] ? action.payload[1].question.category : "",
        createdUser: action.payload[1] ? action.payload[1].question.user : null,
      };
    case QUESTION_EDITOR_UNLOADED:
      return {};
    case QUESTION_SUBMITTED:
      return {
        ...state,
        inProgress: null,
        errors: action.error ? action.payload.errors : null,
      };
    case ASYNC_START:
      if (action.subtype === QUESTION_SUBMITTED) {
        return { ...state, inProgress: true };
      }
    case IMAGE_LOAD:
      return {
        ...state,
        imgUrl: action.payload ? action.payload.url : "",
      };
    case UPDATE_FIELD_EDITOR:
      return { ...state, [action.key]: action.value };
    default:
      return state;
  }
};
