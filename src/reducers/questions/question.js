import {
  QUESTION_PAGE_LOADED,
  QUESTIONS_PAGE_UNLOADED,
  RESOLVE_QUESTION,
  ADD_ANSWER,
  DELETE_ANSWER,
  MARK_ANSWER_AS_RIGHT,
} from "../../constants/actionTypes";
const defaultState = {
  section: "questions",
};
export default (state = defaultState, action) => {
  switch (action.type) {
    case QUESTION_PAGE_LOADED:
      return {
        ...state,
        question: action.payload[0].question,
        answers: action.payload[1] ? action.payload[1].answers : [],
      };
    case QUESTIONS_PAGE_UNLOADED:
      return {};
    case ADD_ANSWER:
      return {
        ...state,
        commentErrors: action.error ? action.payload.errors : null,
        answers: action.error
          ? null
          : (state.answers || []).concat([action.payload.answer]),
      };
    case DELETE_ANSWER:
      const answerId = action.answerId;
      return {
        ...state,
        answers: state.answers.filter((answer) => answer.id !== answerId),
      };
    case RESOLVE_QUESTION:
      return {
        question: action.payload.question,
        answers: action.payload.question?.questionAnswers
          ? action.payload.question.questionAnswers
          : [],
      };
    case MARK_ANSWER_AS_RIGHT:
      return {
        ...state,
        answers: state.answers
          .filter((answer) => answer.id === action.answerId)
          .map((answer) => (answer = action.payload.answer)),
        // answers: (state.answers[
        //   state.answers.filter((answer) => answer.id === action.answerId)
        // ] = action.payload.answer),
      };
    default:
      return state;
  }
};
