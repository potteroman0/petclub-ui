import {
  ADVERTISIMENTS_PAGE_LOADED,
  ADVERTISIMENTS_PAGE_UNLOADED,
} from "../../constants/actionTypes";

export default (state = {}, action) => {
  switch (action.type) {
    case ADVERTISIMENTS_PAGE_LOADED:
      console.log();
      return {
        ...state,
        advertisements: action.payload.advertisements
          ? action.payload.advertisements.data
          : [],
      };
    case ADVERTISIMENTS_PAGE_UNLOADED:
      return {};

    default:
      return state;
  }
};
