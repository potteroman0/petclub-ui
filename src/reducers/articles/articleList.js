import {
  SET_ARTICLE_PAGE,
  APPLY_TAG_FILTER,
  ARTICLES_PAGE_LOADED,
  ARTICLES_PAGE_UNLOADED,
  CHANGE_TAB,
  APPLY_FILTER,
  CREATE_TAG,
  DELETE_TAG,
} from "../../constants/actionTypes";

const defaultState = {
  section: "articles",
  field: "createDate",
  direction: "DESC",
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case ARTICLES_PAGE_LOADED:
      return {
        ...state,
        pager: action.pager,
        hashtags: action.payload[0] ? action.payload[0].hashtags : [],
        articles: action.payload[1] ? action.payload[1].articles.data : [],
        articlesCount: action.payload[1]
          ? action.payload[1].articles.totalElements
          : [],
        currentPage: 0,
        tab: action.tab,
      };
    case ARTICLES_PAGE_UNLOADED:
      return {};
    case CHANGE_TAB:
      return {
        ...state,
        pager: action.pager,
        articles: action.payload ? action.payload.articles.data : [],
        articlesCount: action.payload
          ? action.payload.articles.totalElements
          : 0,
        tab: action.tab,
        currentPage: 0,
        field: defaultState.field,
        direction: defaultState.direction,
        tag: null,
      };
    case SET_ARTICLE_PAGE:
      return {
        ...state,
        articles: action.payload ? action.payload.articles.data : [],
        articlesCount: action.payload
          ? action.payload.articles.totalElements
          : 0,
        currentPage: action.page,
        field: action.filter ? action.filter.field : {},
        direction: action.filter ? action.filter.direction : {},
      };
    case APPLY_TAG_FILTER:
      return {
        ...state,
        pager: action.pager,
        articles: action.payload ? action.payload.articles.data : [],
        articlesCount: action.payload
          ? action.payload.articles.totalElements
          : 0,
        tab: null,
        tag: action.tag,
        field: defaultState.field,
        direction: defaultState.direction,
        currentPage: 0,
      };
    case APPLY_FILTER:
      return {
        ...state,
        articles: action.payload ? action.payload.articles.data : [],
        articlesCount: action.payload
          ? action.payload.articles.totalElements
          : 0,
        filteredTab: action.tab,
        currentPage: 0,
        field: action.filter ? action.filter.field : null,
        direction: action.filter ? action.filter.direction : null,
      };
    case CREATE_TAG:
      return {
        ...state,
        hashtags: action.payload
          ? state.hashtags.concat([action.payload.hashtag])
          : state.hashtags,
      };
    case DELETE_TAG:
      return {
        ...state,
        hashtags: action.payload
          ? state.hashtags.filter((tag) => tag.name !== action.tag)
          : state.hashtags,
      };

    default:
      return state;
  }
};
