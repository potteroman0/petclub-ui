import {
  ARTICLE_PAGE_LOADED,
  ARTICLE_PAGE_UNLOADED,
  ADD_COMMENT,
  DELETE_COMMENT,
  RATE_ARTICLE,
} from "../../constants/actionTypes";

const defaultState = {
  tab: "all",
  section: "articles",
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case ARTICLE_PAGE_LOADED:
      return {
        ...state,
        article: action.payload[0] ? action.payload[0].article : {},
        comments: action.payload[1] ? action.payload[1].comments : [],
        rating: action.payload[0] ? action.payload[0].article.rating : 0.0,
      };
    case ARTICLE_PAGE_UNLOADED:
      return {};
    case ADD_COMMENT:
      return {
        ...state,
        commentErrors: action.error ? action.payload.errors : null,
        comments: action.error
          ? null
          : (state.comments || []).concat([action.payload.comment]),
      };
    case DELETE_COMMENT:
      const commentId = action.commentId;
      return {
        ...state,
        comments: state.comments.filter((comment) => comment.id !== commentId),
      };
    case RATE_ARTICLE:
      return { ...state, rating: action.payload.rating };
    default:
      return state;
  }
};
