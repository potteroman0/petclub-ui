import {
  APPLY_ITEM_CATEGORY_FILTER,
  ITEMS_PAGE_LOADED,
  ITEMS_PAGE_UNLOADED,
  SET_ITEM_PAGE,
  APPLY_ITEM_FILTER,
  CHANGE_SHOP_TAB,
} from "../../constants/actionTypes";

const defaultState = {
  items: [],
  categories: [],
  types: [],
  size: 4,

  sections: "items",
  field: "createDate",
  direction: "DESC",
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case ITEMS_PAGE_LOADED:
      return {
        ...state,
        pager: action.pager,
        items: action.payload[0] ? action.payload[0].items.data : [],
        itemsCount: action.payload[0]
          ? action.payload[0].items.totalElements
          : 0,
        categories: action.payload[1] ? action.payload[1].categories : [],
        types: action.payload[2] ? action.payload[2].types : [],
        currentPage: 0,
        tab: action.tab,
      };
    case ITEMS_PAGE_UNLOADED:
      return {};
    case SET_ITEM_PAGE:
      return {
        ...state,
        items: action.payload ? action.payload.items.data : [],
        itemsCount: action.payload ? action.payload.items.totalElements : 0,
        currentPage: action.page,
      };
    case APPLY_ITEM_CATEGORY_FILTER:
      return {
        ...state,
        items: action.payload ? action.payload.items.data : [],
        itemsCount: action.payload ? action.payload.items.totalElements : 0,
        currentPage: 0,
        category: action.category,
      };
    case APPLY_ITEM_FILTER:
      return {
        ...state,
        items: action.payload ? action.payload.items.data : [],
        itemsCount: action.payload ? action.payload.items.totalElements : 0,
        currentPage: 0,
        category: action.category,
        field: action.filter ? action.filter.field : null,
        direction: action.filter ? action.filter.direction : null,
      };
    case CHANGE_SHOP_TAB:
      console.log("SD");
      return {
        ...state,
        pager: action.pager,
        items: action.payload ? action.payload.items.data : [],
        itemsCount: action.payload ? action.payload.items.totalElements : 0,
        currentPage: 0,
        tab: action.tab,
      };
    default:
      return state;
  }
};
