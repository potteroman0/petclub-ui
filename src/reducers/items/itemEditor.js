import {
  ASYNC_START,
  IMAGE_LOAD,
  ITEM_EDITOR_PAGE_LOADED,
  ITEM_EDITOR_PAGE_UNLOADED,
  ITEM_SAVED,
  UPDATE_FIELD_ITEM,
} from "../../constants/actionTypes";

const defaultState = {
  categories: [],
  types: [],
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case ITEM_EDITOR_PAGE_LOADED:
      return {
        ...state,
        itemId: action.payload[0] ? action.payload[0].item.id : "",
        title: action.payload[0] ? action.payload[0].item.title : "",
        description: action.payload[0]
          ? action.payload[0].item.description
          : "",
        amount: action.payload[0] ? action.payload[0].item.amount : "",
        price: action.payload[0] ? action.payload[0].item.price : "",
        imgUrl: action.payload[0] ? action.payload[0].item.imgUrl : "",
        category: action.payload[0] ? action.payload[0].item.category : "",
        petType: action.payload[0] ? action.payload[0].item.petType : "",
        categories: action.payload[1] ? action.payload[1].categories : [],
        types: action.payload[2] ? action.payload[2].types : [],
      };
    case ITEM_EDITOR_PAGE_UNLOADED:
      return {};
    case ITEM_SAVED:
      return {
        ...state,
        inProgress: null,
        errors: action.error ? action.payload.errors : null,
      };
    case ASYNC_START:
      if (action.subtype === ITEM_SAVED) {
        return { ...state, inProgress: true };
      }
      break;
    case IMAGE_LOAD:
      return {
        ...state,
        imgUrl: action.payload ? action.payload.url : "",
      };
    case UPDATE_FIELD_ITEM:
      return { ...state, [action.key]: action.value };
    default:
      return state;
  }

  return state;
};
