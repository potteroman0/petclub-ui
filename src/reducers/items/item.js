import {
  ITEM_PAGE_LOADED,
  ITEM_PAGE_UNLOADED,
} from "../../constants/actionTypes";

const defaultState = {
  tab: "all",
  section: "items",
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case ITEM_PAGE_LOADED:
      return {
        ...state,
        item: action.payload ? action.payload.item : {},
      };
    case ITEM_PAGE_UNLOADED:
      return {};
    default:
      return state;
  }
};
