import {
  ADD_ITEM_TO_CART,
  CHANGE_AMOUNT,
  REMOVE_ITEM,
} from "../../constants/actionTypes";

const defaultState = {
  itemAmount: [],
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case ADD_ITEM_TO_CART:
      return {
        ...state,
        itemAmount: [
          ...state.itemAmount,
          { item: action.payload, totalPrice: action.payload.price, amount: 1 },
        ],
      };
    case CHANGE_AMOUNT:
      return {
        ...state,
        itemAmount: state.itemAmount.map((priceItem) =>
          priceItem.item.id === action.item.id
            ? {
                item: action.item,
                totalPrice: action.price,
                amount: action.amount,
              }
            : priceItem
        ),
      };
    case REMOVE_ITEM:
      return {
        ...state,
        itemAmount: state.itemAmount.filter(
          (priceItem) => priceItem.item.id !== action.itemId
        ),
      };
    default:
      return state;
  }
};
