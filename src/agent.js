import superagentPromise from "superagent-promise";
import _superagent from "superagent";

const superagent = superagentPromise(_superagent, global.Promise);

const API_ROOT = "https://petclub-be.herokuapp.com";
//const API_ROOT = "http://localhost:8080";

const encode = encodeURIComponent;
const responseBody = (res) => res.body;

let token = null;
const tokenPlugin = (req) => {
  if (token) {
    req.set("Authorization", `Bearer_${token}`);
  }
};

const requests = {
  get: (url) =>
    superagent.get(`${API_ROOT}${url}`).use(tokenPlugin).then(responseBody),
  get: (url, body) =>
    superagent.get(`${API_ROOT}${url}`, body).then(responseBody),
  post: (url, body) =>
    superagent
      .post(`${API_ROOT}${url}`, body)
      .use(tokenPlugin)
      .then(responseBody),
  update: (url, body) =>
    superagent
      .put(`${API_ROOT}${url}`, body)
      .use(tokenPlugin)
      .then(responseBody),
  del: (url) => superagent.del(`${API_ROOT}${url}`).use(tokenPlugin),
};

const Photos = {
  upload: (folder, image) =>
    superagent
      .post(`${API_ROOT}/photos?folder=${folder}`)
      .attach("file", image)
      .use(tokenPlugin)
      .then(responseBody),
};

const Auth = {
  getCurrent: () => requests.post("/auth/user", {}),
  getByUsername: (username) =>
    requests.get(`/users/username/${encode(username)}`),
  login: (username, password) =>
    requests.post("/auth/login", { username, password }),
  register: (username, password, email, phone, imgUrl) =>
    requests.post("/auth/register", {
      username,
      password,
      email,
      phone,
      imgUrl,
    }),
};

const Users = {
  update: (id, username, password, email, phone, imgUrl) =>
    requests.update(`/users/${id}`, {
      username,
      password,
      email,
      phone,
      imgUrl,
    }),
  delete: (id) => requests.del(`/users/${id}`),
};

//Articles func---------------------------------------------------------//
const Tags = {
  getAll: () => requests.get("/hashtags"),
  create: (name) => requests.post("/hashtags", { name }),
  delete: (id) => requests.del(`/hashtags/${id}`),
};

const Articles = {
  getById: (id) => requests.get(`/articles/${id}`),
  getByUserId: (id, size, page, direction, field) =>
    requests.get(`/articles/user/${id}`, { size, page, direction, field }),
  allPaginable: (size, page, direction, field) =>
    requests.get("/articles", { size, page, direction, field }),
  allFilterPaginable: (size, page, direction, field) =>
    requests.get("/articles", { size, page, direction, field }),
  getByHashtag: (hashtag, size, page, direction, field) =>
    requests.get(`/articles/hashtag/${hashtag}`, {
      size,
      page,
      direction,
      field,
    }),

  create: (title, text, hashtags, user) =>
    requests.post("/articles", { title, text, hashtags, user }),
  update: (id, title, text, hashtags, user) =>
    requests.update(`/articles/${id}`, {
      title,
      text,
      hashtags,
      user,
    }),
  delete: (id) => requests.del(`/articles/${id}`),
  rateAricle: (id, userId, rate) =>
    requests.post(`/articles/rate`, {
      rate,
      user: { id: userId },
      article: { id: id },
    }),
};

const Comments = {
  getByArticleId: (id) => requests.get(`/comments/article/${id}`),
  create: (articleId, userId, text, username, imgUrl) =>
    requests.post(`/comments/`, {
      text,
      article: { id: articleId },
      user: { id: userId, username: username, imgUrl: imgUrl },
    }),
  delete: (slug, commentId) => requests.del(`/comments/${commentId}`),
};

//Questions func ----------------------------------------------------//

const Questions = {
  getById: (id) => requests.get(`/questions/${id}`),
  getByUserId: (id, size, page) =>
    requests.get(`/questions/user/${id}`, { size, page }),
  getByCategory: (name, size, page) =>
    requests.get(`/questions/category/${encode(name)}`, { size, page }),
  getAllPaginable: (size, page) => requests.get("/questions", { size, page }),
  getAllCategories: () => requests.get("/questions/categories"),
  getAllResolved: (size, page) =>
    requests.get("/questions/resolved", { size, page }),
  create: (title, text, imgUrl, category, user) =>
    requests.post("/questions", { title, text, imgUrl, category, user }),
  update: (id, title, text, imgUrl, category, user) =>
    requests.update(`/questions/${id}`, {
      title,
      text,
      imgUrl,
      category,
      user,
    }),
  delete: (id) => requests.del(`/questions/${id}`),
  resolve: (id) => requests.update(`/questions/resolve/${id}`, {}),
};

const Answers = {
  getByQuestionId: (id) => requests.get(`/answers/question/${id}`),
  create: (text, questionId, userId) =>
    requests.post("/answers", {
      text,
      user: { id: userId },
      question: { id: questionId },
    }),
  delete: (answerId) => requests.del(`/answers/${answerId}`),
  markAsRight: (commentId) => requests.update(`/answers/mark/${commentId}`),
};

//Avertisement func -----------------------------------------------//

const Advertisements = {
  getById: (id) => requests.get(`/advertisements/${id}`),
  getAllPaginable: (size, page) =>
    requests.get("/advertisements", { size, page }),
  create: (title, description, user) =>
    requests.post("/advertisements", { title, description, user }),
  update: (id, title, description, user) =>
    requests.update(`/advertisements/${id}`, {
      title,
      description,
      user,
    }),
  delete: (id) => requests.del(`/advertisements/${id}`),
};

//Shop func----------------------------------------------------//
const Items = {
  getById: (id) => requests.get(`/shop/items/${id}`),
  getAllItems: (size, page, direction, field) =>
    requests.get(`/shop/items`, { size, page, direction, field }),
  getItemCategories: () => requests.get(`/shop/items/categories`),
  getItemPetTypes: () => requests.get(`/shop/items/petTypes`),
  getItemByCategory: (category, size, page, direction, field) =>
    requests.get(`/shop/items/category/${encode(category)}`, {
      size,
      page,
      direction,
      field,
    }),
  create: (title, description, amount, price, imgUrl, type, petType) =>
    requests.post("/shop/items", {
      title,
      description,
      amount,
      price,
      imgUrl,
      type,
      petType,
    }),
  update: (id, title, description, amount, price, category, imgUrl) =>
    requests.update(`/shop/items/${id}`, {
      title,
      description,
      amount,
      price,
      category,
      imgUrl,
    }),
  delete: (id) => requests.del(`/shop/items/${id}`),
};

const Orders = {
  getByUserId: (id, size, page) =>
    requests.get(`/orders/user/${id}`, { size, page }),
};

export default {
  Articles,
  Auth,
  Comments,
  Tags,
  Photos,
  Users,
  Questions,
  Answers,
  Advertisements,
  Items,
  Orders,
  setToken: (_token) => {
    token = _token;
  },
};
